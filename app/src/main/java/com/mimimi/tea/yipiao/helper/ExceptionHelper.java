package com.mimimi.tea.yipiao.helper;

/**
 * Created by Vilkazz on 2018-03-07.
 */

public class ExceptionHelper {
    public static void throwClassCastException(String expected, String received) {
        throw new ClassCastException(expected + " object expected, got " + received + " instead");
    }
}
