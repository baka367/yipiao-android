package com.mimimi.tea.yipiao.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class ParamsHelper {

    private static Context appContext;

    private final static String PREFERENCE_FILE_KEY = "mhb_main_preference_key";

    public static void Init(Context ctx) {
        appContext = ctx;
    }

    private static String getParamKey(Params param, Object[] variations) {
        String key = param.getKey();//+ (param.getLocalized() ? "_" + LocalizationHelper.GetLanguage() : "");
        if (variations == null)
            return key;
        for (int i = 0; i < variations.length; i++) {
            key = key.replace("{" + i + "}", variations[i].toString());
        }
        return key;
    }

    public static void SetApplicationParameters(Params param, Object value) {
        SetApplicationParameters(param, value, null);
    }

    public static void SetApplicationParameters(Params param, Object value, Object[] variations) {
        String key = getParamKey(param, variations);
        handleExpiration(key, param.getExpire(), param.getPreferenceFileKey());
        SharedPreferences sharedPref = appContext.getSharedPreferences(param.getPreferenceFileKey(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        switch (param.getType()) {
            case "string":
                editor.putString(key, (String) value);
                break;
            case "int":
                editor.putInt(key, (int) value);
                break;
            case "long":
                editor.putLong(key, (long) value);
                break;
            case "boolean":
                editor.putBoolean(key, (Boolean) value);
                break;
            case "float":
                editor.putFloat(key, (float) value);
                break;
        }
        editor.apply();
        LogHelper.getInstance().logEvent("local params", "Set: " + key);
        LogHelper.getInstance().logEvent("local params", "Val: " + value);
    }

    private static void handleExpiration(String key, long time, String preferenceKey) {
        if (time > 0) {
            SharedPreferences sharedPref = appContext.getSharedPreferences(preferenceKey, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong(key + "_expire", System.currentTimeMillis() + time);
            editor.apply();
        }
    }

    private static boolean isApplicationParameterValid(String key, long time, String preferenceKey) {
        if (time > 0) {
            SharedPreferences sharedPref = appContext.getSharedPreferences(preferenceKey, Context.MODE_PRIVATE);
            long currentTime = System.currentTimeMillis();
            long expire = sharedPref.getLong(key + "_expire", currentTime - 1);
            return currentTime < expire;
        } else {
            return true;
        }
    }

    public static Object GetApplicationParameters(Params param, Object defaultVal) {
        return GetApplicationParameters(param, null, defaultVal);
    }

    public static Object GetApplicationParameters(Params param, Object[] variations, Object defaultVal) {
        String key = getParamKey(param, variations);
        if (!isApplicationParameterValid(key, param.getExpire(), param.getPreferenceFileKey()))
            return defaultVal;

        SharedPreferences sharedPref = appContext.getSharedPreferences(param.getPreferenceFileKey(), Context.MODE_PRIVATE);
        Object returnVal;
        switch (param.getType()) {
            case "string":
                returnVal = sharedPref.getString(key, (String) defaultVal);
                break;
            case "int":
                returnVal = sharedPref.getInt(key, (int) defaultVal);
                break;
            case "long":
                returnVal = sharedPref.getLong(key, (long) defaultVal);
                break;
            case "boolean":
                returnVal = sharedPref.getBoolean(key, (Boolean) defaultVal);
                break;
            case "float":
                returnVal = sharedPref.getFloat(key, (float) defaultVal);
                break;
            default:
                returnVal = "";
                break;
        }
        LogHelper.getInstance().logEvent("local params", "Get: " + key);
        LogHelper.getInstance().logEvent("local params", "Val: " + returnVal);
        return returnVal;
    }

    public static void ClearApplicationParametersByPreferenceKey(Params param) {
        SharedPreferences sharedPref = appContext.getSharedPreferences(param.getPreferenceFileKey(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

    public static void ClearApplicationParameter(Params param) {
        ClearApplicationParameter(param, null);
    }

    public static void ClearApplicationParameter(Params param, Object[] variations) {
        SharedPreferences sharedPref = appContext.getSharedPreferences(param.getPreferenceFileKey(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        String key = getParamKey(param, variations);
        editor.remove(key);
        editor.remove(key + "_expire");
        editor.apply();
    }

    public static boolean IsApplicationParameterValid(Params param) {
        return IsApplicationParameterValid(param, null);
    }

    public static boolean IsApplicationParameterValid(Params param, Object[] variations) {
        String key = getParamKey(param, variations);
        return isApplicationParameterValid(key, param.getExpire(), param.getPreferenceFileKey());

    }

    public enum Params {
        SERVER_CLIENT_TIME_DIFFERENCE("server_client_time_difference", "long", -1),
        USER_TRACKING_LAST_SENT("user_tracking_last_sent", "long", -1),
        APPLICATION_ENVIRONMENT("application_environment", "string", -1),
        ACCESS_TOKEN("access_token", "string", -1),
        UID("uid", "string", -1),
        APP_WAS_ALREADY_LAUNCHED("appWasAlreadyLaunched", "boolean", -1),
        PUSH_NOTIFICATION_ENABLED("push_notifications_enabled", "boolean", -1),
        USERNAME("username", "string", -1),
        USER_IMAGE("profile_image", "string", -1),;

        private final String key;
        private final String type;
        private final String preferenceFile;
        private final Long expire;

        Params(String a, String b, long d) {
            key = a;
            type = b;
            expire = d;
            preferenceFile = PREFERENCE_FILE_KEY;
        }

        Params(String a,
               String b,
               long d,
               String e) {
            key = a;
            type = b;
            expire = d;
            preferenceFile = e;
        }

        public String getKey() {
            return key;
        }

        public String getType() {
            return type;
        }

        public String getPreferenceFileKey() {
            return preferenceFile;
        }

        public long getExpire() {
            return expire;
        }
    }
}
