package com.mimimi.tea.yipiao.controller.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.adapter.TeaListAdapter;
import com.mimimi.tea.yipiao.controller.activity.MyProfileActivity;
import com.mimimi.tea.yipiao.model.tea.TeaList;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.ProfileRequest;

public class ProfileTeaListFragment extends Fragment{
    private static final String TAG = "PROFILE TEA LIST";
    GridView profileTeaListFragment;
    TeaListAdapter teaListAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        profileTeaListFragment = (GridView) inflater.inflate(R.layout.fragment_profile_tea_list, container, false);

        MyProfileActivity activity = (MyProfileActivity)getContext();

        GridView teaList = profileTeaListFragment.findViewById(R.id.tea_grid);
        if (activity != null) {
            teaListAdapter = new TeaListAdapter(getContext());
            teaList.setAdapter(teaListAdapter);
            requestTeaList();
        }

        return profileTeaListFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // newInstance constructor for creating fragment with arguments
    public static ProfileTeaListFragment newInstance() {
        return new ProfileTeaListFragment();
    }

    public void requestTeaList() {
        new ProfileRequest(getContext()).getFavouriteTeas(new CustomRequest.ApiResponse<TeaList>() {
            @Override
            public void onSuccess(TeaList data) {
                teaListAdapter.addItems(data.getTeas());
            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }
}
