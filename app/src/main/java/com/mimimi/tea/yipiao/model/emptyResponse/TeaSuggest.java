package com.mimimi.tea.yipiao.model.emptyResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mimimi.tea.yipiao.model.Error;

import java.util.List;

public class TeaSuggest {
    @SerializedName("errors")
    @Expose
    private List<Error> errors;

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
