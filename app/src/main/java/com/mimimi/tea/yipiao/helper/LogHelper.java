package com.mimimi.tea.yipiao.helper;

import android.util.Log;

public class LogHelper {

    public void logEvent(String arg1, Object arg2) {
        if (UtilHelper.appIsInDebugMode()) {
            if (arg2 instanceof String)
                Log.d(arg1, (String) arg2);
            else
                Log.d(arg1, "" + arg2);
        }
    }

    public void logEvent(String arg1, String arg2, Throwable arg3) {
        if (UtilHelper.appIsInDebugMode())
            Log.e(arg1, arg2, arg3);
    }

    private LogHelper() {
    }

    private static LogHelper mInstance = null;

    public static LogHelper getInstance() {
        if (mInstance == null) {
            mInstance = new LogHelper();
        }
        return mInstance;
    }
}