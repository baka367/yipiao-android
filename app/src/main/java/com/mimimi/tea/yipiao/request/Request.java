package com.mimimi.tea.yipiao.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mimimi.tea.yipiao.model.User;

public class Request<T> {

    @SerializedName("data")
    @Expose
    private T data;
    @SerializedName("user")
    @Expose
    private User user;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
