package com.mimimi.tea.yipiao.model.Note;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NoteList {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("notes")
    @Expose
    private List<NoteListItem> notes = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<NoteListItem> getNotes() {
        return notes;
    }

    public void setNotes(List<NoteListItem> notes) {
        this.notes = notes;
    }
}
