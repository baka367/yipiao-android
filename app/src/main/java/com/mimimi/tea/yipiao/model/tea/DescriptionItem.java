package com.mimimi.tea.yipiao.model.tea;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DescriptionItem {

    public static final int TYPE_TEXT = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_TITLE = 2;
    public static final int TYPE_MAIN_TITLE = 3;

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("type")
    @Expose
    private String type;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static int getViewCount() {
        return 4;
    }

    public int getTypeId(){
        switch (type) {
            case "paragraph":
                return TYPE_TEXT;
            case "image":
                return TYPE_IMAGE;
            case "title":
                return TYPE_TITLE;
            case "main_title":
                return TYPE_MAIN_TITLE;
            default:
                return -1;
        }
    }
}
