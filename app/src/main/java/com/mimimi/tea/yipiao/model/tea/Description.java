package com.mimimi.tea.yipiao.model.tea;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Description {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("items")
    @Expose
    private List<DescriptionItem> items = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DescriptionItem> getItems() {
        return items;
    }

    public void setItems(List<DescriptionItem> items) {
        this.items = items;
    }

}