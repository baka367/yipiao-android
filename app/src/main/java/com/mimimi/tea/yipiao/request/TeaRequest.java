package com.mimimi.tea.yipiao.request;

import android.content.Context;

import com.mimimi.tea.yipiao.YipiaoApplication;
import com.mimimi.tea.yipiao.model.BaseModel;
import com.mimimi.tea.yipiao.model.tea.Tea;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class TeaRequest extends CustomRequest {

    private ApiInterface api;
    public TeaRequest(Context context) {
        super(context);
        api = YipiaoApplication.restAdapter.create(ApiInterface.class);
    }

    private interface ApiInterface {
        @GET("/api/tea/get/{id}")
        Call<BaseModel<Tea>> getTea(@Path("id") int teaId);
    }

    public CustomRequest getTea(int id, final ApiResponse<Tea> listener) {
        completeContentCall(api.getTea(id), listener);
        return this;
    }
}
