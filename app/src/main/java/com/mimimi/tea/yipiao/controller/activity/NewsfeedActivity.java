package com.mimimi.tea.yipiao.controller.activity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.Services.TrackingService;
import com.mimimi.tea.yipiao.adapter.NewsfeedPagerAdapter;
import com.mimimi.tea.yipiao.controller.fragment.NoteFragment;
import com.mimimi.tea.yipiao.controller.fragment.TeaDetailsFragment;
import com.mimimi.tea.yipiao.listener.EndlessViewPagerListener;
import com.mimimi.tea.yipiao.listener.OnSwipeTouchListener;
import com.mimimi.tea.yipiao.model.newsfeed.NewsfeedTeaList;
import com.mimimi.tea.yipiao.model.tea.Note;
import com.mimimi.tea.yipiao.model.tea.Tea;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.NewsfeedRequest;
import com.mimimi.tea.yipiao.view.TopBar;

import java.util.HashMap;

public class NewsfeedActivity extends AbstractActivity implements NoteFragment.OnFragmentInteractionListener, NoteFragment.OnNoteUpdateListener, TeaDetailsFragment.OnTeaDetailsAnimationListener{
    private static final String TAG = "NewsfeedPager Activity";

    private RelativeLayout rootLayout;
    private ViewPager newsfeedPager;
    private NewsfeedPagerAdapter newsfeedAdapter;

    private RelativeLayout previousButtonHolder;
    private RelativeLayout nextButtonHolder;
    private RelativeLayout detailsButtonHolder;
    public TopBar topBarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsfeed);

        overridePendingTransition(R.anim.activity_slide_in_from_bottom, R.anim.activity_slide_in_from_top);

        topBarView = new TopBar(this, TopBar.LEFT_BUTTON_PROFILE, TopBar.RIGHT_BUTTON_SEARCH, TopBar.MIDDLE_BUTTON_NONE);

        rootLayout = findViewById(R.id.activity_newsfeed);
        rootLayout.addView(topBarView);

        createView();
        setUpButtons();

        newsfeedAdapter = new NewsfeedPagerAdapter(NewsfeedActivity.this.getSupportFragmentManager());

        newsfeedPager = findViewById(R.id.newsfeed_pager);
        newsfeedPager.setAdapter(newsfeedAdapter);
        newsfeedPager.addOnPageChangeListener(new EndlessViewPagerListener() {
            @Override
            public boolean onLoadMore(int page, int limit) {
                return false;
            }
            @Override
            public void onPageSelected(int position) {
                setNewsfeedButtonVisibility(newsfeedPager.getCurrentItem());
            }
        });

        newsfeedPager.setRotationY(180);

        requestNewsfeedList(0, 8);
    }

    private void createView() {

        ImageView buttonPrevious = new ImageView(this);
        buttonPrevious.setImageResource(R.drawable.ico_bglight_arrow_left);

        previousButtonHolder = new RelativeLayout(this);
        previousButtonHolder.setId(View.generateViewId());
        previousButtonHolder.addView(buttonPrevious);

        RelativeLayout.LayoutParams previousButtonParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.standard_button_dimen),
                (int) getResources().getDimension(R.dimen.standard_button_dimen)
        );
        previousButtonParams.addRule(RelativeLayout.CENTER_VERTICAL);
        previousButtonParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        previousButtonParams.setMarginStart((int)getResources().getDimension(R.dimen.padding15));
        previousButtonHolder.setLayoutParams(previousButtonParams);
        rootLayout.addView(previousButtonHolder);


        ImageView buttonNext = new ImageView(this);
        buttonNext.setImageResource(R.drawable.ico_bglight_arrow_right);

        nextButtonHolder = new RelativeLayout(this);
        nextButtonHolder.setId(View.generateViewId());
        nextButtonHolder.addView(buttonNext);

        RelativeLayout.LayoutParams nextButtonParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.standard_button_dimen),
                (int) getResources().getDimension(R.dimen.standard_button_dimen)
        );
        nextButtonParams.addRule(RelativeLayout.CENTER_VERTICAL);
        nextButtonParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        nextButtonParams.setMarginEnd((int)getResources().getDimension(R.dimen.padding15));
        nextButtonHolder.setLayoutParams(nextButtonParams);
        rootLayout.addView(nextButtonHolder);

        ImageView buttonDetails = new ImageView(this);
        buttonDetails.setBackground(getResources().getDrawable(R.drawable.ico_bglight_list_godown, null));
        buttonDetails.setFocusable(true);

        detailsButtonHolder = new RelativeLayout(this);
        detailsButtonHolder.setId(View.generateViewId());
        detailsButtonHolder.addView(buttonDetails);

        RelativeLayout.LayoutParams detailsButtonParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.standard_button_dimen),
                (int) getResources().getDimension(R.dimen.standard_button_dimen)
        );
        detailsButtonParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        detailsButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        detailsButtonParams.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.news_feed_godown_button_bottom_padding));
        detailsButtonHolder.setLayoutParams(detailsButtonParams);
        rootLayout.addView(detailsButtonHolder);
    }

    private void setUpButtons() {
            nextButtonHolder.setVisibility(View.GONE);
            nextButtonHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newsfeedPager.setCurrentItem(newsfeedPager.getCurrentItem() - 1);
                }
            });
            previousButtonHolder.setVisibility(View.GONE);
            previousButtonHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newsfeedPager.setCurrentItem(newsfeedPager.getCurrentItem() + 1);
                }
            });
        detailsButtonHolder.setOnTouchListener(null);
        detailsButtonHolder.setOnTouchListener(new OnSwipeTouchListener(this) {
            public void onSwipeTop() {
                showTeaDetails();
            }
            public void onClick() {
                showTeaDetails();
            }
        });
    }

    private void showTeaDetails() {
        toggleNewsfeedButtons(false);
        Tea teaToShow = getCurrentTea();
        showTeaDetails(teaToShow, false);

        HashMap<String, Integer> trackerParams = new HashMap<>();
        trackerParams.put("tea_id", teaToShow.getId());
        this.logTracking(TrackingService.TYPE_ENGAGEMENT, TrackingService.ENG_TYPE_TEA_NEWSFEED, trackerParams);
    }

    private Tea getCurrentTea() {
        return (newsfeedAdapter.getCurrentTea(newsfeedPager.getCurrentItem())).getTea();
    }
    private void setCurrentTea(Tea tea) {
        newsfeedAdapter.setCurrentTea(newsfeedPager.getCurrentItem(), tea);
    }

    private void requestNewsfeedList(int page, int limit) {
        new NewsfeedRequest(this).getNewsfeedList(page, limit, new CustomRequest.ApiResponse<NewsfeedTeaList>() {
            @Override
            public void onSuccess(NewsfeedTeaList data) {
                newsfeedAdapter.addItems(data.getFeaturedTeaList());
                setNewsfeedButtonVisibility(newsfeedPager.getCurrentItem());
            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }

    private void setNewsfeedButtonVisibility(int position) {
        if (newsfeedAdapter.isFirst(position)) {
            nextButtonHolder.setVisibility(View.GONE);
        } else {
            if (nextButtonHolder.getVisibility() != View.VISIBLE) {
                nextButtonHolder.setVisibility(View.VISIBLE);
            }
        }
        if (newsfeedAdapter.isLast(position)) {
            previousButtonHolder.setVisibility(View.GONE);
        } else {
            if (previousButtonHolder.getVisibility() != View.VISIBLE) {
                previousButtonHolder.setVisibility(View.VISIBLE);
            }
        }
       showButtonFadeIn();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void toggleNewsfeedButtons(boolean showButtons) {
        if (showButtons) {
            detailsButtonHolder.setVisibility(View.INVISIBLE);
            setNewsfeedButtonVisibility(newsfeedPager.getCurrentItem());
        } else {
            showButtonFadeOut();
        }
    }

    @Override
    public void updateNote(String updatedNote) {
        Tea tea = getCurrentTea();
        Note note = tea.getNote();
        note.setText(updatedNote);
        tea.setNote(note);
        setCurrentTea(tea);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void showButtonFadeIn() {
        Handler newsfeedButtonHandler = new Handler();
        newsfeedButtonHandler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        final Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
                        fadeIn.setDuration(200);
                        if (detailsButtonHolder.getVisibility() == View.INVISIBLE) {
                            detailsButtonHolder.setVisibility(View.VISIBLE);
                            detailsButtonHolder.setAnimation(fadeIn);
                        }
                        if (previousButtonHolder.getVisibility() == View.INVISIBLE) {
                            previousButtonHolder.setVisibility(View.VISIBLE);
                            previousButtonHolder.setAnimation(fadeIn);
                        }
                        if (nextButtonHolder.getVisibility() == View.INVISIBLE) {
                            nextButtonHolder.setVisibility(View.VISIBLE);
                            nextButtonHolder.setAnimation(fadeIn);
                        }
//                        if (topBarView.getVisibility() == View.GONE) {
//                            topBarView.setVisibility(View.VISIBLE);
//                            topBarView.setAnimation(fadeIn);
//                        }

                    }
                }
                ,(long)(Constants.TEA_DETAILS_ANIMATION_DURATION * 0.5));

    }

    public void showButtonFadeOut() {
        Handler newsfeedButtonHandler = new Handler();
        newsfeedButtonHandler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        final Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
                        fadeOut.setDuration(270);
                        if (detailsButtonHolder.getVisibility() == View.VISIBLE) {
                            detailsButtonHolder.setVisibility(View.GONE);
                            detailsButtonHolder.setAnimation(fadeOut);
                        }
                        if (previousButtonHolder.getVisibility() == View.VISIBLE) {
                            previousButtonHolder.setVisibility(View.GONE);
                            previousButtonHolder.setAnimation(fadeOut);
                        }
                        if (nextButtonHolder.getVisibility() == View.VISIBLE) {
                            nextButtonHolder.setVisibility(View.GONE);
                            nextButtonHolder.setAnimation(fadeOut);
                        }
//                        if (topBarView.getVisibility() == View.VISIBLE) {
//                            topBarView.setVisibility(View.GONE);
//                            topBarView.setAnimation(fadeOut);
//                        }

                    }
                }
                ,(long)(Constants.TEA_DETAILS_ANIMATION_DURATION * 0.5));

    }

    @Override
    public void updateViewOnTeaDetailsAnimation(boolean isExiting) {
        toggleNewsfeedButtons(isExiting);
//        if (!isExiting) {
//            showButtonFadeOut();
//        } else {
//            showButtonFadeIn();
//        }
    }
}
