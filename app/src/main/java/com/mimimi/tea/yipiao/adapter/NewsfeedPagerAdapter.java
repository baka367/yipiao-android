package com.mimimi.tea.yipiao.adapter;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mimimi.tea.yipiao.controller.fragment.NewsfeedFragment;
import com.mimimi.tea.yipiao.controller.fragment.ProfileNoteListFragment;
import com.mimimi.tea.yipiao.controller.fragment.ProfileTeaListFragment;
import com.mimimi.tea.yipiao.model.newsfeed.NewsfeedTea;
import com.mimimi.tea.yipiao.model.tea.Note;
import com.mimimi.tea.yipiao.model.tea.Tea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NewsfeedPagerAdapter extends FragmentPagerAdapter {
    @SuppressLint("UseSparseArrays")
    private HashMap<Integer, Fragment> fragmentHashMap = new HashMap<>();

    private ArrayList<NewsfeedTea> newsfeedList;

    public NewsfeedPagerAdapter(FragmentManager fm) {
        super(fm);
        newsfeedList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        NewsfeedFragment ft;
        if (fragmentHashMap.get(position) != null) {
            ft = (NewsfeedFragment)fragmentHashMap.get(position);
            return ft;
        }

        ft = NewsfeedFragment.newInstance();
        ft.setNewsfeed(getCurrentTea(position));
        fragmentHashMap.put(position, ft);
        return ft;
    }

    public NewsfeedTea getCurrentTea(int position) {
        return newsfeedList.get(position);
    }

    @Override
    public int getCount() {
        return newsfeedList.size();
    }

    public void addItems (List<NewsfeedTea> newNewsfeedTeas){
        newsfeedList.addAll(newNewsfeedTeas);
        this.notifyDataSetChanged();
    }

    public boolean isFirst(int position) {
        return position == 0;
    }

    public boolean isLast(int position) {
        return position == newsfeedList.size() - 1;
    }

    public void setCurrentTea(int position, Tea tea) {
        NewsfeedTea currentNewsfeedTea = getCurrentTea(position);
        currentNewsfeedTea.setTea(tea);
        newsfeedList.set(position, currentNewsfeedTea);
    }
}
