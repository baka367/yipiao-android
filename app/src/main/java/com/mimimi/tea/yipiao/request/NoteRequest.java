package com.mimimi.tea.yipiao.request;

import android.content.Context;

import com.mimimi.tea.yipiao.YipiaoApplication;
import com.mimimi.tea.yipiao.model.BaseModel;
import com.mimimi.tea.yipiao.model.emptyResponse.NoteWrite;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public class NoteRequest extends CustomRequest {

    private NoteRequest.ApiInterface api;

    public NoteRequest(Context context) {
        super(context);
        api = YipiaoApplication.restAdapter.create(NoteRequest.ApiInterface.class);
    }

    private interface ApiInterface {
        @POST("/api/note/write")
        Call<BaseModel<NoteWrite>> writeNote(@Body HashMap<String, Object> body);
    }

    public CustomRequest writeNote(HashMap<String, Object> body, final ApiResponse<NoteWrite> listener) {
        completeContentCall(api.writeNote(body), listener);
        return this;
    }
}
