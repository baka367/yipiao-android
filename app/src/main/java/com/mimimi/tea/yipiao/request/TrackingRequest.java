package com.mimimi.tea.yipiao.request;

import android.content.Context;

import com.mimimi.tea.yipiao.YipiaoApplication;
import com.mimimi.tea.yipiao.model.BaseModel;
import com.mimimi.tea.yipiao.model.emptyResponse.NoteWrite;
import com.mimimi.tea.yipiao.model.emptyResponse.Tracking;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public class TrackingRequest extends CustomRequest {

    private TrackingRequest.ApiInterface api;

    public TrackingRequest(Context context) {
        super(context);
        api = YipiaoApplication.restAdapter.create(TrackingRequest.ApiInterface.class);
    }

    private interface ApiInterface {
        @POST("/api/tracking")
        Call<BaseModel<Tracking>> tracking(@Body HashMap<String, Object> body);
    }

    public CustomRequest tracking(HashMap<String, Object> body, final ApiResponse<Tracking> listener) {
        completeContentCall(api.tracking(body), listener);
        return this;
    }
}
