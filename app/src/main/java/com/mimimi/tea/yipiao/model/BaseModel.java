package com.mimimi.tea.yipiao.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.*;
import java.lang.Error;
import java.util.List;

public class BaseModel<T> {

    @SerializedName("data")
    @Expose
    private T data;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("errors")
    @Expose
    private List<Error> errors;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
