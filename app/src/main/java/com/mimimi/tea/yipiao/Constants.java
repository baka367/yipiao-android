package com.mimimi.tea.yipiao;

import com.google.gson.internal.Streams;
import com.mimimi.tea.yipiao.helper.ParamsHelper;


public class Constants {
    public static String APP_ID = "wx91c9342e450189aa";
    public static String APP_SECRET_KEY = "7e754cf91dbc8f146a45efccc0ad874d";
    public static final String ENV_PROD = "prod";
    public static final String ENV_DEV = "dev";
    private static final String DEV_URL = "http://139.199.79.14";

    public static final String WEIXIN_APP_ID = "wx91c9342e450189aa";

//    private static final String DEV_URL = "http://www.yipiao.me";
    private static final String PROD_URL = "foo";

    public static final int RESULT_LOGIN_ERROR = 3;
    public static final int RESULT_LOGIN_SUCCESS = 1;
    public static final int RESULT_LOGIN_CANCELED = 2;

    public static int TEA_DETAILS_ANIMATION_DURATION = 450;

    public static final String ACCEPT_HEADER = "application/yipiao.api+json;version=1";
    public static final String IMAGE_STORE_BASE_URL = "http://oqi0g5mfs.bkt.clouddn.com/";
    public static final String APP_DOWNLOAD_BASE_URL = "www.yipiao.me/download";
    public static final String TEA_SHARE_BASE_URL = "www.yipiao.me/tea/";

    public static final String KEY_TEA_ID = "teaId";
    public static final String KEY_TEA_IMAGE = "teaImage";
    public static final String KEY_TEA_IS_FAVOURITE= "teaFavourite";

    public static final int REQ_CODE_UPDATE_TEA_FAVOURITE_STATE = 201;

    public static final String FONT_DIN_REGULAR = "fonts/DIN-Regular.otf";
    public static final String FONT_PING_FANG_SEMIBOLD = "fonts/PingFangSemibold.ttf";
    public static final String FONT_PING_FANG_EXTRA_LIGHT = "fonts/PingFangUltraLight.ttf";
    public static final String FONT_PING_FANG_HEAVY = "fonts/PingFangHeavy.ttf";
    public static final String FONT_PING_FANG_LIGHT = "fonts/PingFangLight.ttf";
    public static final String FONT_PING_FANG_MEDIUM = "fonts/PingFangMedium.ttf";
    public static final String FONT_PING_FANG_REGULAR = "fonts/PingFangRegular.ttf";
    public static final String FONT_SONGTI_SC_REGULAR = "fonts/STSongti-SC-Regular.ttf";
    public static final String FONT_SONGTI_SC_BLACK = "fonts/STSongti-SC-Black.ttf";
    public static final String FONT_SONGTI_SC_BOLD = "fonts/STSongti-SC-Bold.ttf";
    public static final String FONT_SONGTI_TC_REGULAR = "fonts/STSongti-TC-Regular.ttf";
    public static final String FONT_SONGTI_TC_BLACK = "fonts/STSongti-TC-Black.ttf";
    public static final String FONT_SONGTI_TC_BOLD = "fonts/STSongti-TC-Bold.ttf";
    public static final String FONT_SONG = "fonts/STSong.ttf";

    public static final String LOGIN_REQUESTED_CLASS_NAME = "requested_class";
    public static final String LOGIN_REQUESTED_CLASS_BUNDLE = "requested_bundle";
    public static final String KEY_ACTIVITY_WAS_CLOSED = "activityWasClosed";

    public static final int LOGIN_WECHAT = 1;
    public static final int LOGIN_MOBILE = 2;
    public static final int LOGIN_MOBILE_NEW_USER = 3;

    public static String getBaseUrl() {
        String url = PROD_URL;
        switch (getEnvironment()) {
            case ENV_DEV:
                url = DEV_URL;
                break;
            case ENV_PROD:
                url = PROD_URL;
                break;
        }
        return url;
    }

    private static String getEnvironment() {
        return (String) ParamsHelper.GetApplicationParameters(
                ParamsHelper.Params.APPLICATION_ENVIRONMENT,
                ENV_PROD);
    }

    public static void setEnvironment(String env) {
        ParamsHelper.SetApplicationParameters(
                ParamsHelper.Params.APPLICATION_ENVIRONMENT,
                env);
    }
}
