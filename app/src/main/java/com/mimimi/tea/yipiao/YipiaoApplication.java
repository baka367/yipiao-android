package com.mimimi.tea.yipiao;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.algolia.search.saas.Client;
import com.algolia.search.saas.Index;
import com.androidnetworking.AndroidNetworking;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mimimi.tea.yipiao.helper.LogHelper;
import com.mimimi.tea.yipiao.helper.ParamsHelper;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class YipiaoApplication extends Application {
    private static final String TAG = "YipiaoApplication";

    public static Context applicationContext;
    public static String DeviceVersion = "";
    public static String DeviceId = "";
    private static String AppVersion = "";

    public static String getAppVersion() {
        return AppVersion;
    }

    public static void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    public static Retrofit restAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        applicationContext = getApplicationContext();

        MultiDex.install(applicationContext);
        //======================INIT======================
        ParamsHelper.Init(getBaseContext());
        Constants.setEnvironment("dev");
        //todo implement this asap
//        TrackingHelper.getInstance().Init(getApplicationContext());
        AndroidNetworking.initialize(getApplicationContext());
//        initFabric();
        initFonts();
        initVectorSupportPreLolipop();
        initLeakCanary();

        initAlgolia();
//        initTalkingDataAppAnalytics();
//        initTalkingDataAdAnalytics();
//        Constants.setUpWeChatIDs();

        preloadFonts();



        LogHelper.getInstance().logEvent(TAG, "onCreate: BuildConfig.DEBUG " + BuildConfig.DEBUG);

        setUpRetrofit();
        //======================DeviceCode======================
//        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
//        final String tmDevice, tmSerial, androidId;
//        tmDevice = "" + tm.getDeviceId();
//        tmSerial = "" + tm.getSimSerialNumber();
//        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
//        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
//        deviceUuid.toString();
        DeviceId = "and-" + Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        DeviceVersion = "android_" + android.os.Build.VERSION.RELEASE;

        //=====================TRACKING============================
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            setAppVersion("Android-" + pInfo.versionName + ":" + pInfo.versionCode);
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        }

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        Tracker tracker = analytics.newTracker("UA-59585510-4");
        tracker.enableExceptionReporting(true);
        tracker.enableAutoActivityTracking(true);
        //========================JPUSH===============================
//        JPushInterface.setDebugMode(true);
//        JPushInterface.init(this);

//        Intent i = new Intent(this, BackgroundService.class);
//        startService(i);

//        UserHelper.updateJpushStatusForUser();


        //todo verify software buttons
//        var decorView = window.getDecorView();
//        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION //REMOVE THIS LINE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void initLeakCanary() {
        if (UtilHelper.appIsInDebugMode()) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(this);
            // Normal app init code...
        }
    }

    private void initVectorSupportPreLolipop() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void initFonts() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/DIN-Regular.otf")
//                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void preloadFonts() {
        //todo laggy, find ways to optimize later
        Typeface aFont;
        aFont = UtilHelper.getFont(this, Constants.FONT_PING_FANG_LIGHT);
        aFont = UtilHelper.getFont(this, Constants.FONT_PING_FANG_MEDIUM);
        aFont = UtilHelper.getFont(this, Constants.FONT_SONGTI_SC_BOLD);

    }

    private void initAlgolia() {
//        Client client = new Client("YourApplicationID", "YourAPIKey");
//        Index index = client.getIndex("your_index_name");
    }

//    private void initFabric() {
//        Fabric.with(applicationContext, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(RandomHelper.appIsInDebugMode()).build()).build());
//        Fabric.with(applicationContext, new Crashlytics());
//    }

    private void setUpRetrofit() {
        File httpCacheDir = null;
        Cache cache = null;
        try {
            httpCacheDir = new File(getApplicationContext().getCacheDir(), "httpResponses");
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            cache = new Cache(httpCacheDir, httpCacheSize);

        } catch (Exception e) {
            Log.e("Retrofit", "Could not create http cache", e);
        }
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.interceptors().add(interceptor);
        httpClient.connectTimeout(10, TimeUnit.SECONDS);
        httpClient.readTimeout(10, TimeUnit.SECONDS);
        httpClient.writeTimeout(10, TimeUnit.SECONDS);
        httpClient.cache(cache);

        if (UtilHelper.appIsInDebugMode()) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(loggingInterceptor);
        }

        OkHttpClient okHttpClient = httpClient.build();

        Gson gson = new GsonBuilder().create();

        restAdapter = new Retrofit.Builder()
                .baseUrl(Constants.getBaseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogHelper.getInstance().logEvent(TAG, "setUpRetrofit: " + Constants.getBaseUrl());
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            String accessToken = UserHelper.getAccessToken();

            String url = request.url().toString();
            if (url.indexOf('?') > 0) {
                url = url.substring(0, url.indexOf('?'));
            }
            LogHelper.getInstance().logEvent(TAG, "intercept: " + url);

            request = request.newBuilder()
//                    .addHeader("Access-Key", "AccessKey")
                    .addHeader("Accept", Constants.ACCEPT_HEADER)
                    .addHeader("Device-Id", YipiaoApplication.DeviceId)
                    .addHeader("Device-Version", YipiaoApplication.DeviceVersion)
                    .addHeader("App-Version", getAppVersion())
                    .addHeader("Authorization", accessToken)
//                    .addHeader("Content-Language", "zh-CN")
                    .build();
            return chain.proceed(request);
        }
    };

    public void resetRetrofit() {
        setUpRetrofit();
    }
}
