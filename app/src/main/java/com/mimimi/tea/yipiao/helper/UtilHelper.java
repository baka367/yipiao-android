package com.mimimi.tea.yipiao.helper;

import android.app.Activity;
import android.app.BuildConfig;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.TouchDelegate;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.controller.fragment.NoteFragment;
import com.mimimi.tea.yipiao.controller.fragment.TeaDetailsFragment;
import com.mimimi.tea.yipiao.model.tea.Tea;

import java.util.concurrent.ExecutionException;

import de.hdodenhof.circleimageview.CircleImageView;
import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

public class UtilHelper {
    private static final String TAG = "UtilHelper";
    private static Gson gson;

    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int convertDpToPixels(int dimensionResourceId, Context context) {
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                context.getResources().getDimension(dimensionResourceId),
                r.getDisplayMetrics());
    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static String getRandomString(int len) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < len; i++) {
            String charSet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM1234567890";
            str.append(charSet.charAt((int) Math.floor(Math.random() * charSet.length())));
        }
        return str.toString();
    }

    public static int getRandomNumber(int min, int max) {
        return min + (int) Math.floor(Math.random() * (max - min + 1));
    }

    public static String getCroppedImage(int width, int height) {
        return "?imageView2/3/w/" + width + "/h/" + height;
    }

    public static String getOnlyWidthCroppedImage(int width) {
        return "?imageView2/3/w/" + width;
    }

    public static void expandTouchArea(final View parentView, final View childView) {
        parentView.post(new Runnable() {
            @Override
            public void run() {
                Rect rect = new Rect();
                childView.getHitRect(rect);
                rect.top -= AppConstantsHelper.INCREASED_AREA_OF_CLICK;
                rect.left -= AppConstantsHelper.INCREASED_AREA_OF_CLICK;
                rect.right += AppConstantsHelper.INCREASED_AREA_OF_CLICK;
                rect.bottom += AppConstantsHelper.INCREASED_AREA_OF_CLICK;
                parentView.setTouchDelegate(new TouchDelegate(rect, childView));
            }
        });
    }
//
//    public static boolean checkIsUserIsLoggedIn(Activity activity) {
//        if (UserHelper.requireLogin(activity, true)) {
//            activity.finish();
//            return false;
//        }
//        return true;
//    }

    public static boolean appIsInDebugMode() {
        return true;
//        return BuildConfig.DEBUG;
    }

    public static void rotateAnimation(View view) {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setRepeatMode(Animation.RESTART);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(1000);
        view.startAnimation(rotateAnimation);
    }

    public static void changeFont(TextView textView, Typeface font) {
        textView.setTypeface(font);
    }

    public static void showKeyboard(Activity activity) {
        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).
                toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static Typeface getFont(Context context, String fontName) {
        return Typeface.createFromAsset(context.getAssets(), fontName);
    }

    public static void setTopAndGradientBackground(GradientDrawable.Orientation orientation, int colorTopBackground, int[] colorGradientBackground, View topBackground, View gradientBackground) {
        topBackground.setBackgroundColor(colorTopBackground);
        GradientDrawable drawable = new GradientDrawable(orientation, colorGradientBackground);
        gradientBackground.setBackground(drawable);
    }

    public static int getColor(String stringColor) {
        int color;
        try {
            color = Color.parseColor(stringColor);
        } catch (Exception e) {
            if (e instanceof IllegalArgumentException) {
                color = Color.parseColor("#" + stringColor);
            } else
                color = Color.parseColor("#00000000");
        }
        return color;
    }

    public static void setBackgroundImage(Context context, final View view, String imageUrl) {
        Glide.with(context)
                .load(Constants.IMAGE_STORE_BASE_URL + imageUrl + UtilHelper.getCroppedImage(getScreenWidth(context)/2, getScreenHeight(context)/2))
                .asBitmap()
                .thumbnail(0.1f)
                .into(new SimpleTarget<Bitmap>(getScreenWidth(context), getScreenHeight(context)) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        Log.i(TAG, "Finished loading cover image");
                        int sdk = android.os.Build.VERSION.SDK_INT;
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            view.setBackgroundDrawable(new BitmapDrawable(resource));
                        } else {
                            view.setBackground(new BitmapDrawable(resource));
                        }
                    }
                });
    }

    public static void setImage(Context context, final ImageView view, String imageUrl) {
        Glide.with(context)
                .load(Constants.IMAGE_STORE_BASE_URL + imageUrl + UtilHelper.getCroppedImage(getScreenWidth(context)/2, getScreenHeight(context)/2))
                .asBitmap()
                .thumbnail(0.1f)
                .into(new SimpleTarget<Bitmap>(getScreenWidth(context), getScreenHeight(context)) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        Log.i(TAG, "Finished loading cover image");
                        int sdk = android.os.Build.VERSION.SDK_INT;
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            view.setImageDrawable(new BitmapDrawable(resource));
                        } else {
                            view.setImageDrawable(new BitmapDrawable(resource));
                        }
                    }
                });
    }

    public static void setRoundImage(Context context, final CircleImageView view, String imageUrl) {
        Glide.with(context)
                .load(Constants.IMAGE_STORE_BASE_URL + imageUrl + UtilHelper.getCroppedImage(view.getWidth()/2, view.getHeight()/2))
                .asBitmap()
                .thumbnail(0.1f)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        Log.i(TAG, "Finished loading profile image");
                        view.setImageDrawable(new BitmapDrawable(resource));
                    }
                });
    }

    /**
     *
     * Converts drawable to bitmap and resizes based on the given ratio
     *
     * @param drawable input drawable
     * @param resizeRatio resize ratio
     * @return bitmap
     */
    public static Bitmap convertToBitmap(Drawable drawable, double resizeRatio) {
        if (drawable == null) return null;
        int newHeight = (int) (drawable.getBounds().height() * resizeRatio);
        int newWidth = (int) (drawable.getBounds().width() * resizeRatio);
        Bitmap mutableBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.getBounds().height();
        drawable.setBounds(0, 0, newWidth, newHeight);
        drawable.draw(canvas);

        return mutableBitmap;
    }

    public static Gson getGsonParser() {
        if(null == gson) {
            GsonBuilder builder = new GsonBuilder();
            gson = builder.create();
        }
        return gson;
    }
}
