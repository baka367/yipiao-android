package com.mimimi.tea.yipiao.helper;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.Services.TrackingService;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.controller.fragment.ShareFragment;
import com.mimimi.tea.yipiao.model.tea.Tea;
import com.mimimi.tea.yipiao.wxapi.WXShare;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

/**
 * Processes sharing content to weixin and to clipboard
 */
public class ShareHelper {
    public static final int SHARE_TYPE_APP = 0;
    public static final int SHARE_TYPE_TEA = 1;

    /**
     *
     *  Shows share fragment. Handles all logic regarding link preparation
     *
     * @param context context
     * @param type share type
     * @param shareObject share object
     * @param <T> currently either null or Tea
     */
    public static <T> void showShareFragment(Context context, int type, T shareObject) {
        FragmentTransaction ft = ((AbstractActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((AbstractActivity) context).getSupportFragmentManager().findFragmentByTag("share");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack("share");

        HashMap<String, String> shareBlockDetails = new HashMap<>();

        switch (type) {
            case SHARE_TYPE_APP:
                shareBlockDetails.put("link", Constants.APP_DOWNLOAD_BASE_URL);
                shareBlockDetails.put("title", context.getString(R.string.app_name_cn));
                shareBlockDetails.put("text", context.getString(R.string.tea_share_text_generic));
                break;
            case SHARE_TYPE_TEA:
                if (shareObject instanceof Tea) {
                    shareBlockDetails.put("teaId", ((Tea) shareObject).getId().toString());
                    shareBlockDetails.put("link", Constants.TEA_SHARE_BASE_URL + ((Tea) shareObject).getId());
                    shareBlockDetails.put("title", ((Tea) shareObject).getName());
                    shareBlockDetails.put("text", context.getString(R.string.tea_share_text_generic));
                    shareBlockDetails.put("image", ((Tea) shareObject).getCoverImage());
                } else {
                    ExceptionHelper.throwClassCastException("Tea", shareObject.getClass().getName());
                }
                break;
            default:
                // invalid share type. We do nothing
                return;
        }

        SupportBlurDialogFragment shareFragment = ShareFragment.newInstance(type, shareBlockDetails);
        shareFragment.show(ft, "share");

        trackShareEvent(context, type, shareObject);
    }

    /**
     * Sharing data tracker
     *
     * @param context context
     * @param type share type
     * @param shareObject share object
     * @param <T> currently either null or Tea
     */
    private static <T> void trackShareEvent(Context context, int type, T shareObject) {
        int trackingEvent;
        HashMap<String, Integer> trackerParams = new HashMap<>();

        switch (type) {
            case SHARE_TYPE_APP:
                trackingEvent = TrackingService.ENG_TYPE_SHARE_APP;
                break;
            case SHARE_TYPE_TEA:
                trackingEvent = TrackingService.ENG_TYPE_SHARE_TEA;
                if (shareObject instanceof Tea) {
                    trackerParams.put("tea_id", ((Tea)(shareObject)).getId());
                } else {
                    ExceptionHelper.throwClassCastException("Tea", shareObject.getClass().getName());
                }
                break;
            default:
                //nothing to track. invalid type
                return;
        }
        ((AbstractActivity) context).logTracking(TrackingService.TYPE_ENGAGEMENT, trackingEvent, trackerParams);
    }

    /**
     *
     * Function that handles weixin sharing (both in message and moments
     *
     * @param shareType currently app or tea
     * @param shareData a hashmap with weixin share object data
     * @param context context
     * @param isMoments share to moments or chat
     * @param shareImage image to display in weixin. Can be overriden for certain share types
     */
    public static void weixinLink(Context context, int shareType, HashMap<String, String> shareData, boolean isMoments, Bitmap shareImage) {

        switch (shareType) {
            case SHARE_TYPE_APP:
                shareImage =
                        BitmapFactory.decodeResource(context.getResources(), R.drawable.yipiao_icon_small);
                break;
            case SHARE_TYPE_TEA:
                    if (shareImage == null) {
                        shareImage =
                                BitmapFactory.decodeResource(context.getResources(), R.drawable.yipiao_icon_small);
                    }
                break;
            default:
                shareImage =
                        BitmapFactory.decodeResource(context.getResources(), R.drawable.yipiao_icon_small);
        }

        WXShare wxShare = new WXShare(context);
        wxShare.shareHtml(
                shareData.get("link"),
                shareData.get("title"),
                shareData.get("text"),
                shareImage,
                isMoments
        );
    }

    /**
     *
     * Function handles sharing by copying share link to clipboard
     *
     * @param context context
     */
    public static void plainLink(Context context, int shareType, HashMap<String, String> shareData) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);

        String shareLink;

        switch (shareType) {
            case SHARE_TYPE_APP:
                shareLink = Constants.APP_DOWNLOAD_BASE_URL;
                break;
            case SHARE_TYPE_TEA:
                shareLink = shareData != null ? shareData.get("link") : Constants.APP_DOWNLOAD_BASE_URL;
                break;
            default:
                shareLink = Constants.APP_DOWNLOAD_BASE_URL;
        }

        ClipData clip = ClipData.newPlainText("link", shareLink);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        Toast.makeText(context, "链接复制到剪贴板", Toast.LENGTH_LONG).show();
    }
}
