package com.mimimi.tea.yipiao.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.Services.TrackingService;
import com.mimimi.tea.yipiao.controller.activity.MyProfileActivity;
import com.mimimi.tea.yipiao.controller.fragment.TeaDetailsFragment;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.tea.Tea;
import com.mimimi.tea.yipiao.model.tea.TeaListItem;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.TeaRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class TeaListAdapter extends BaseAdapter {

    private static final String TAG = "Tea list adapter";
    private Context context;
    private MyProfileActivity activity;
    private List<TeaListItem> teaList;

    public TeaListAdapter(Context context) {
        this.context = context;
        this.activity = (MyProfileActivity) context;
        teaList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return teaList.size();
    }

    @Override
    public TeaListItem getItem(int position) {
        return teaList.get(position);
    }

    public void addItems(List<TeaListItem> items) {
        teaList.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return teaList.get(position).getId();
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        TeaHolder aTeaHolder;
        if (convertView == null) {
            aTeaHolder= new TeaListAdapter.TeaHolder();
            convertView = inflater.inflate(R.layout.adapter_tea_layout, null);
            aTeaHolder.teaName = convertView.findViewById(R.id.tea_list_tea_title_text_view);
            aTeaHolder.teaType = convertView.findViewById(R.id.tea_list_tea_type_text_view);

            aTeaHolder.teaName.setTypeface(UtilHelper.getFont(context, Constants.FONT_SONGTI_SC_BOLD));
            aTeaHolder.teaType.setTypeface(UtilHelper.getFont(context, Constants.FONT_SONGTI_SC_REGULAR));

            int cellWidth = (UtilHelper.getScreenWidth(context) - UtilHelper.convertDpToPixels(R.dimen.padding15, context)) / 2;
            convertView.setLayoutParams(new LinearLayout.LayoutParams(cellWidth, (int)(cellWidth*0.8084)));

            convertView.setTag(aTeaHolder);
        } else {
            aTeaHolder = (TeaHolder) convertView.getTag();
        }

        final TeaListItem tea = getItem(position);

        aTeaHolder.teaName.setText(tea.getName());
        aTeaHolder.teaType.setText(tea.getType());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TeaRequest(context).getTea(tea.getId(), new CustomRequest.ApiResponse<Tea>() {
                    @Override
                    public void onSuccess(Tea data) {
                        activity.showTeaDetails(data, true);

                        HashMap<String, Integer> trackerParams = new HashMap<>();
                        trackerParams.put("tea_id", tea.getId());
                        activity.logTracking(TrackingService.TYPE_ENGAGEMENT, TrackingService.ENG_TYPE_TEA_PROFILE, trackerParams);
                    }

                    @Override
                    public void onFail(String message) {
                        Log.e(TAG, message);
                    }
                });
            }
        });

        return convertView;
    }

    static class TeaHolder {
        TextView teaName;
        TextView teaType;
    }
}
