package com.mimimi.tea.yipiao.listener;

import android.support.v4.view.ViewPager;

import com.mimimi.tea.yipiao.controller.activity.NewsfeedActivity;


public abstract class EndlessViewPagerListener implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public abstract boolean onLoadMore(int page, int limit);
}
