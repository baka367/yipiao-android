package com.mimimi.tea.yipiao.model.Note;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mimimi.tea.yipiao.model.tea.Note;
import com.mimimi.tea.yipiao.model.tea.Tea;

public class NoteListItem {
    @SerializedName("note")
    @Expose
    private Note note;
    @SerializedName("tea")
    @Expose
    private Tea tea;

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Tea getTea() {
        return tea;
    }

    public void setTea(Tea tea) {
        this.tea = tea;
    }

}
