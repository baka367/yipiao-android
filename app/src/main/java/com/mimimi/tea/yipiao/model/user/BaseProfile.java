package com.mimimi.tea.yipiao.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseProfile {

    @SerializedName("nickname")
    @Expose
    private String nickname;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("fav_tea_count")
    @Expose
    private Integer favTeaCount;
    @SerializedName("note_count")
    @Expose
    private Integer noteCount;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getFavTeaCount() {
        return favTeaCount;
    }

    public void setFavTeaCount(Integer favTeaCount) {
        this.favTeaCount = favTeaCount;
    }

    public Integer getNoteCount() {
        return noteCount;
    }

    public void setNoteCount(Integer noteCount) {
        this.noteCount = noteCount;
    }
}
