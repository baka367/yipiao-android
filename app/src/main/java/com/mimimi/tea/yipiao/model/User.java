package com.mimimi.tea.yipiao.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("logged_in")
    @Expose
    private Boolean loggedIn = false;
    @SerializedName("receive_push_notifications")
    @Expose
    private Boolean receivePushNotifications = true;

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }


    public Boolean getReceivePushNotifications() {
        return receivePushNotifications;
    }

    public void setReceivePushNotifications(Boolean receivePushNotifications) {
        this.receivePushNotifications = loggedIn;
    }
}
