package com.mimimi.tea.yipiao.helper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.controller.activity.LoginActivity;
import com.mimimi.tea.yipiao.controller.activity.NewsfeedActivity;
import com.mimimi.tea.yipiao.model.User;

import java.util.ArrayList;

public final class UserHelper {

    private static LoginActivity latestLoginActivity;
    public static ArrayList<Activity> activities = new ArrayList<>();

    public static void registerLoginActivity(LoginActivity activity) {
        latestLoginActivity = activity;
    }

    public static void addToActivitiesArray(Activity activity) {
        activities.add(activity);
    }

    private static final String JPUSH_GUEST_ALIAS = "guest";
    private static final String TAG = "UserHelper";
    private static String cartHash;

    public static String getCartHash() {
        return cartHash;
    }

    public static void setCartHash(String cartHashs) {
        cartHash = cartHashs;
    }

    public static void setUserInfo(String accessToken, String uid) {
        setAccessToken(accessToken);
        setUid(uid);
    }


    public static String getAccessToken() {
        return (String) ParamsHelper.GetApplicationParameters(ParamsHelper.Params.ACCESS_TOKEN, "");

    }

    public static boolean getPushNotificationsEnabled() {
        return (boolean) ParamsHelper.GetApplicationParameters(ParamsHelper.Params.PUSH_NOTIFICATION_ENABLED, true);
    }

    public static String getUsername() {
        return (String) ParamsHelper.GetApplicationParameters(ParamsHelper.Params.USERNAME, "用户名");
    }

    public static String getProfileImage() {
        return (String) ParamsHelper.GetApplicationParameters(ParamsHelper.Params.USER_IMAGE, "");
    }

    public static String getUid() {
        return (String) ParamsHelper.GetApplicationParameters(ParamsHelper.Params.UID, "");

    }

    public static boolean isGuest() {
        String at = getAccessToken();
        return at == null || at.isEmpty();
    }

    private static void setAccessToken(String accessToken) {
        ParamsHelper.SetApplicationParameters(ParamsHelper.Params.ACCESS_TOKEN, accessToken);
    }

    public static void setPushNotificationsEnabled(boolean isPushNotificationsEnabled) {
        ParamsHelper.SetApplicationParameters(ParamsHelper.Params.PUSH_NOTIFICATION_ENABLED, isPushNotificationsEnabled);
    }
    public static void setUsername(String username) {
        ParamsHelper.SetApplicationParameters(ParamsHelper.Params.USERNAME, username);
    }
    public static void setProfileImage(String profileImage) {
        ParamsHelper.SetApplicationParameters(ParamsHelper.Params.USER_IMAGE, profileImage);
    }

    public static void setFirstTimeLaunch(boolean value) {
        ParamsHelper.SetApplicationParameters(ParamsHelper.Params.APP_WAS_ALREADY_LAUNCHED, value);
    }

    public static boolean isAppFirstTimeLaunch() {
        return (Boolean) ParamsHelper.GetApplicationParameters(ParamsHelper.Params.APP_WAS_ALREADY_LAUNCHED, true);
    }

    private static void setUid(String uid) {
        ParamsHelper.SetApplicationParameters(ParamsHelper.Params.UID, uid);
    }

    public static void logoutUser(Activity activity) {
        setUserInfo("", "");
        setFirstTimeLaunch(true);
        activity.startActivity(
                new Intent(activity, NewsfeedActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
        );
    }

    public static boolean requireLogin(Activity activity, boolean activityWasClosed) {
        if (isGuest()) {
            String className;
            Class<?> enclosingClass = activity.getClass().getEnclosingClass();
            if (enclosingClass != null) {
                className = enclosingClass.getName();
            } else {
                className = activity.getClass().getName();
            }
            Bundle extras = activity.getIntent().getExtras();
            activity.startActivity(
                    new Intent(activity, LoginActivity.class)
                            .putExtra(Constants.LOGIN_REQUESTED_CLASS_NAME, className)
                            .putExtra(Constants.LOGIN_REQUESTED_CLASS_BUNDLE, extras)
                            .putExtra((Constants.KEY_ACTIVITY_WAS_CLOSED), activityWasClosed));
            return true;
        }
        return false;
    }

    public static void clearLoginActivitiesAndHideKeyboard(Activity activity, int result, int whatTypeOfLogin) {
        UtilHelper.hideKeyboard(activity);

        for (int i = 0; i < activities.size(); i++) {
            if (activities.get(i) != null)
                activities.get(i).finish();
        }
        activities.clear();

//        if (!UtilHelper.appIsInDebugMode())
//            Answers.getInstance().logCustom(new LoginTracker(result, whatTypeOfLogin));



        if (latestLoginActivity != null) {
            latestLoginActivity.handleLoginResult(result);
        }
    }

    public static void updateUserData(User user) {
        if (user == null) return;
        if (getPushNotificationsEnabled() != user.getReceivePushNotifications()) {
            setPushNotificationsEnabled(user.getReceivePushNotifications());
        }
    }


    public static int getPassedId(Activity activity, String key) {
        int productId = -1;
        if (activity.getIntent().getExtras() != null)
            productId = activity.getIntent().getExtras().getInt(key);

        return productId;
    }

    public static boolean getPassedBooleanValue(Activity activity, String key) {
        boolean value = false;
        if (activity.getIntent().getExtras() != null)
            value = activity.getIntent().getExtras().getBoolean(key);

        return value;
    }

    public static String getPassedString(Activity activity, String key) {
        String id = "";
        if (activity.getIntent().getExtras() != null)
            id = activity.getIntent().getExtras().getString(key);

        return id;
    }


    public static String getPassedModel(Activity activity, String key) {
        String model = "N/A";
        if (activity.getIntent().getExtras() != null)
            model = activity.getIntent().getExtras().getString(key);

        return model;
    }
    public static void showFragmentDialog(AbstractActivity activity, DialogFragment fragment) {
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction().addToBackStack(null);
        fragment.show(fragmentTransaction, activity.getClass().getName());
    }

    public static void showPopupFragmentDialog(final AbstractActivity activity, final DialogFragment fragment) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction().addToBackStack(null);
                fragment.show(fragmentTransaction, activity.getClass().getName());
            }
        });
    }
}
