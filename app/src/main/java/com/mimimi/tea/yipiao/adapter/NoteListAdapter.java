package com.mimimi.tea.yipiao.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.Note.NoteListItem;

import java.util.ArrayList;
import java.util.List;


public class NoteListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NoteListItem> noteList;

    public NoteListAdapter(Context context) {
        this.context = context;
        noteList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return noteList.size();
    }

    @Override
    public NoteListItem getItem(int position) {
        return noteList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return noteList.get(position).getNote().getId();
    }

    public void addItems(List<NoteListItem> items) {
        noteList.addAll(items);
        notifyDataSetChanged();
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        NoteListAdapter.NoteHolder aNoteHolder;
        if (convertView == null) {
            aNoteHolder= new NoteListAdapter.NoteHolder();
            convertView = inflater.inflate(R.layout.adapter_note_layout, null);

            aNoteHolder.teaName = convertView.findViewById(R.id.note_tea_name_text_view);
            aNoteHolder.updateDate = convertView.findViewById(R.id.note_date_text_view);
            aNoteHolder.note = convertView.findViewById(R.id.note_short_details_text_view);

            aNoteHolder.teaName.setTypeface(UtilHelper.getFont(context, Constants.FONT_SONGTI_SC_REGULAR));
            aNoteHolder.updateDate.setTypeface(UtilHelper.getFont(context, Constants.FONT_SONGTI_SC_BOLD));
            aNoteHolder.note.setTypeface(UtilHelper.getFont(context, Constants.FONT_PING_FANG_LIGHT));

            convertView.setTag(aNoteHolder);
        } else {
            aNoteHolder = (NoteListAdapter.NoteHolder) convertView.getTag();
        }
        final NoteListItem note = getItem(position);

        aNoteHolder.teaName.setText(note.getTea().getName());
        aNoteHolder.updateDate.setText(note.getNote().getUpdatedAt());
        aNoteHolder.note.setText(note.getNote().getText());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //object manipulation to fit the data model
            note.getTea().setNote(note.getNote());
            ((AbstractActivity) context).showNoteFragment(note.getTea());
            }
        });

        return convertView;
    }

    public void clear() {
        //clear adapter before adding new items
        noteList.clear();
    }

    static class NoteHolder {
        TextView teaName;
        TextView updateDate;
        TextView note;
        }
}
