package com.mimimi.tea.yipiao.request;

import android.content.Context;

import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.model.BaseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class CustomRequest {
    private Context context;

    public interface ApiResponse<T> {
        void onSuccess(T data);

        void onFail(String message);
    }

    public CustomRequest(Context context) {
        this.context = context;
    }

    <T> void completeContentCall(Call<BaseModel<T>> call, final ApiResponse<T> listener) {
        completeCall(call, listener);
    }

    <T> void completeBareCall(Call<T> call, final ApiResponse<T> listener) {
        completeCall(call, listener);
    }

    @SuppressWarnings("unchecked")
    private <T, G> void completeCall(Call<T> call, final ApiResponse<G> listener) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                T model = response.body();
                if (model instanceof BaseModel) {
                    UserHelper.updateUserData(((BaseModel) model).getUser());
                    listener.onSuccess((G) ((BaseModel) model).getData());
                } else if (response.code() == 204) {
                    listener.onSuccess((G) model);
                } else if (response.errorBody() != null) {

                    listener.onFail("Error handling not yet implemented");
                }
                //todo put something here so the app doesnt hang
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                int a = 1;
            }
        });
    }
}
