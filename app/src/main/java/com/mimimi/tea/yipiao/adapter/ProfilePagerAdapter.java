package com.mimimi.tea.yipiao.adapter;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mimimi.tea.yipiao.controller.fragment.ProfileNoteListFragment;
import com.mimimi.tea.yipiao.controller.fragment.ProfileTeaListFragment;

import java.util.HashMap;

public class ProfilePagerAdapter extends FragmentPagerAdapter {
    @SuppressLint("UseSparseArrays")
    private HashMap<Integer, Fragment> fragmentHashMap = new HashMap<>();

    private static int FRAGMENT_COUNT = 2;
    private int favTeaCount = 0;
    private int noteCount = 0;

    public ProfilePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (fragmentHashMap.get(position) != null) {
            return fragmentHashMap.get(position);
        }

        switch (position) {
            case 0:
                ProfileTeaListFragment ft = ProfileTeaListFragment.newInstance();
                fragmentHashMap.put(position, ft);
                return ft;
            case 1:
                ProfileNoteListFragment fn = ProfileNoteListFragment.newInstance();
                fragmentHashMap.put(position, fn);
                return fn;
        }

        return null;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        //todo pass the parameter with profile api
        //https://stackoverflow.com/questions/32031437/how-do-i-change-the-text-style-of-a-selected-tab-when-using-tablayout/40903654#40903654
        switch (position) {
            case 0:
                return "收藏 " + favTeaCount;
            case 1:
                return "笔记 " + noteCount;
        }
        return null;
    }

    public void setListHeaders (int newFavTeaCount, int newNoteCount){
        favTeaCount = newFavTeaCount;
        noteCount = newNoteCount;
        this.notifyDataSetChanged();
    }
}
