package com.mimimi.tea.yipiao.controller.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.controller.fragment.NoteFragment;
import com.mimimi.tea.yipiao.controller.fragment.ProfileFragment;
import com.mimimi.tea.yipiao.controller.fragment.ProfileNoteListFragment;
import com.mimimi.tea.yipiao.controller.fragment.ProfileSettingsFragment;
import com.mimimi.tea.yipiao.controller.fragment.TeaDetailsFragment;
import com.mimimi.tea.yipiao.controller.fragment.TeaSuggestFragment;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.model.user.BaseProfile;
import com.mimimi.tea.yipiao.model.user.UploadImage;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.ProfileRequest;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;

public class MyProfileActivity extends AbstractActivity implements NoteFragment.OnFragmentInteractionListener, NoteFragment.OnNoteUpdateListener, TeaSuggestFragment.OnFragmentInteractionListener, TeaDetailsFragment.OnTeaDetailsAnimationListener{
    private static final String TAG = "My profile Activity";
    RelativeLayout rootLayout;

    private static final int STATE_PROFILE = 1;
    private static final int STATE_SETTINGS = 2;
    private static int profileState = STATE_PROFILE;

    BaseProfile profileData;
    public ProfileFragment profileFragment;
    ProfileSettingsFragment profileSettingsFragment;

    @Override
    public void updateViewOnTeaDetailsAnimation(boolean isExiting) {

    }

    public interface ProfileStateManager{
        public void setState(int newState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        if (findViewById(R.id.profile_fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }
            requestBaseUserProfile();
            showProfileFragment();
        }
    }

    public void showSettingsFragment() {   // Create a new Fragment to be placed in the activity layout
        profileSettingsFragment = new ProfileSettingsFragment();

        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments
        profileSettingsFragment.setArguments(getIntent().getExtras());

        // Add the fragment to the 'fragment_container' FrameLayout
//        .setTransition(R.anim.enter_from_right)
        getSupportFragmentManager().beginTransaction().addToBackStack("profile-settings")
                .add(R.id.profile_fragment_container, profileSettingsFragment).commit();
    }

    public void showProfileFragment() {   // Create a new Fragment to be placed in the activity layout
        // Create a new Fragment to be placed in the activity layout
        profileFragment = new ProfileFragment();

        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments
        profileFragment.setArguments(getIntent().getExtras());


        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.profile_fragment_container, profileFragment).commit();
    }

    public void requestBaseUserProfile() {
        new ProfileRequest(this).getBaseProfile(new CustomRequest.ApiResponse<BaseProfile>() {
            @Override
            public void onSuccess(BaseProfile data) {
                //passes the favTea/note counts
                profileData = data;

                UserHelper.setProfileImage(data.getAvatar());
                UserHelper.setUsername(data.getNickname());
                profileFragment.updateView();
            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }

    public BaseProfile getProfileData() {
        return profileData;
    }

    @Override
    public void updateNote(String updatedNote) {

        ((ProfileNoteListFragment)profileFragment.adapterViewPager.getItem(1)).requestNoteList();
        requestBaseUserProfile();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    bitmap.extractAlpha();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                new ProfileRequest(this).updateImage(resultUri, new CustomRequest.ApiResponse<UploadImage>() {
                    @Override
                    public void onSuccess(UploadImage data) {
                        if (data != null) {
                            if (profileData != null && data.getAvatar() != null) {
                                UserHelper.setProfileImage(data.getAvatar());
                                if (profileFragment != null){
                                    profileFragment.updateView();
                                }
                                if (profileSettingsFragment != null){
                                    profileSettingsFragment.updateView();
                                }

                            }
                        } else {
                            Toast.makeText(getParent(), "Image upload failed", Toast.LENGTH_LONG).show();
                            //show toast that image upload has failed
                            Log.e(TAG, "image upload failed");
                        }
                    }
                    @Override
                    public void onFail(String message) {
                        Toast.makeText(getParent(), "Image upload failed", Toast.LENGTH_LONG).show();
                        Log.e(TAG, message);
                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

}
