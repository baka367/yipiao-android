package com.mimimi.tea.yipiao.request;

import android.content.Context;

import com.mimimi.tea.yipiao.YipiaoApplication;
import com.mimimi.tea.yipiao.model.BaseModel;
import com.mimimi.tea.yipiao.model.tea.TeaLike;

import retrofit2.Call;
import retrofit2.http.PATCH;
import retrofit2.http.Path;


public class UserRequest extends CustomRequest {

    private UserRequest.ApiInterface api;

    public UserRequest(Context context) {
        super(context);
        api = YipiaoApplication.restAdapter.create(UserRequest.ApiInterface.class);
    }

    private interface ApiInterface {
        @PATCH("/api/profile/tea-like/{id}")
        Call<BaseModel<TeaLike>> likeTea(@Path("id") int teaId);
    }

    public CustomRequest likeTea(int teaId, final ApiResponse<TeaLike> listener) {
        completeContentCall(api.likeTea(teaId), listener);
        return this;
    }
}
