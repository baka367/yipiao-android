package com.mimimi.tea.yipiao.controller.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.adapter.NoteListAdapter;
import com.mimimi.tea.yipiao.model.Note.NoteList;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.ProfileRequest;

public class ProfileNoteListFragment extends Fragment {
    private static final String TAG = "NOTE LIST";
    NoteListAdapter noteListAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ListView noteList = (ListView) inflater.inflate(R.layout.fragment_profile_note_list, container, false);
        noteListAdapter = new NoteListAdapter(getContext());
        noteList.setAdapter(noteListAdapter);

        requestNoteList();


        return noteList;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // newInstance constructor for creating fragment with arguments
    public static ProfileNoteListFragment newInstance() {
        return new ProfileNoteListFragment();
    }

    public void requestNoteList() {
        new ProfileRequest(getContext()).getNoteList(new CustomRequest.ApiResponse<NoteList>() {
            @Override
            public void onSuccess(NoteList data) {
                noteListAdapter.clear();
                noteListAdapter.addItems(data.getNotes());
            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }
}
