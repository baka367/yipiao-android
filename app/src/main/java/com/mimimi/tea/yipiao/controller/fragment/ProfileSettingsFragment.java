package com.mimimi.tea.yipiao.controller.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.controller.activity.MyProfileActivity;
import com.mimimi.tea.yipiao.helper.ShareHelper;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.emptyResponse.ProfileUpdate;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.ProfileRequest;
import com.mimimi.tea.yipiao.view.TopBar;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileSettingsFragment extends Fragment {
    private static final String TAG = "Profile settings frag";

    RelativeLayout rootLayout;
    AbstractActivity activity;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_profile_settings, container, false);
        TopBar topBarView = new TopBar(getContext(), TopBar.LEFT_BUTTON_BACK_FRAGMENT, TopBar.RIGHT_BUTTON_NONE, TopBar.MIDDLE_BUTTON_NONE);
        rootLayout.addView(topBarView);

        //prevent clickthrough
        rootLayout.setClickable(true);

        activity = ((AbstractActivity) getActivity());

        initButtons();
        setUpImageView();
        setUpUsername();
        initVersionNumber();
        return rootLayout;
    }

    private void initButtons() {
        TextView logoutButton = rootLayout.findViewById(R.id.log_out_btn);
        logoutButton.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserHelper.logoutUser(getActivity());
            }
        });

        TextView notificationSwitchLabel = rootLayout.findViewById(R.id.enable_notifications_tw);
        notificationSwitchLabel.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        SwitchCompat notificationSwitch = rootLayout.findViewById(R.id.enable_notifications_switch);
        notificationSwitch.setChecked(UserHelper.getPushNotificationsEnabled());
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                profileUpdate(isChecked, UserHelper.getUsername());
            }
        });

        TextView appStoreButtonLabel = rootLayout.findViewById(R.id.leave_feedback_tw);
        appStoreButtonLabel.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        TextView appStoreButton = rootLayout.findViewById(R.id.leave_feedback_btn);
        appStoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity.getBaseContext(), "Store feedback not implemented ...yey..", Toast.LENGTH_LONG).show();
            }
        });

        TextView shareButtonLabel = rootLayout.findViewById(R.id.recommend_to_friends_tw);
        shareButtonLabel.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        TextView shareButton = rootLayout.findViewById(R.id.recommend_to_friends_btn);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.showShareFragment(getContext(), ShareHelper.SHARE_TYPE_APP, null);
            }
        });


        TextView suggestTeaButtonLabel = rootLayout.findViewById(R.id.suggest_tea_tw);
        suggestTeaButtonLabel.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        TextView suggestTeaButton = rootLayout.findViewById(R.id.suggest_tea_btn);
        suggestTeaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showTeaSuggestFragment();
            }
        });
    }

    public void initVersionNumber() {
        TextView versionStringTw = rootLayout.findViewById(R.id.version_number_tw);
        versionStringTw.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        try {
            versionStringTw.append(activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void setUpImageView() {
        if (UserHelper.isGuest()) {
            return;
        }
        CircleImageView profileImage = rootLayout.findViewById(R.id.profile_image);
        UtilHelper.setRoundImage(getContext(), profileImage, UserHelper.getProfileImage());
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setAspectRatio(1,1)
                        .setAllowFlipping(false)
                        .setAllowRotation(false)
                        .start(activity);
            }
        });
    }

    private void setUpUsername() {
        TextView profileUserNameTw = rootLayout.findViewById(R.id.profile_user_name);
        profileUserNameTw.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        profileUserNameTw.setText(UserHelper.getUsername());
        profileUserNameTw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUsernameUpdateDialog();
            }
        });
    }

    public void updateView() {

        setUpImageView();
    }

    public void showUsernameUpdateDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.new_nickname);
        // I'm using fragment here so I'm using getView() to provide ViewGroup
        // but you can provide here any other instance of ViewGroup from your Fragment / Activity
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog_username_update, (ViewGroup) getView(), false);
        // Set up the input
        final EditText input = viewInflated.findViewById(R.id.username_input_tw);
        input.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        input.setText(UserHelper.getUsername());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

        // Set up the buttons
        builder.setPositiveButton(getString(R.string.button_confirm_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String newNickname = input.getText().toString();
                profileUpdate(UserHelper.getPushNotificationsEnabled(), newNickname);
            }
        });
        builder.setNegativeButton(getString(R.string.button_cancel_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.show();

        TextView alertDialogTitle = alertDialog.findViewById(android.R.id.message);
        Button buttonOk = alertDialog.findViewById(android.R.id.button1);
        Button buttonCancel = alertDialog.findViewById(android.R.id.button2);

        alertDialogTitle.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        buttonOk.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
        buttonCancel.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_REGULAR));
    }

    public void profileUpdate(final boolean receivePushNotifications, final String newNickname) {
        HashMap<String, Object> profileJson = new HashMap<>();
        profileJson.put("receive_push_notifications", receivePushNotifications);
        profileJson.put("nickname", newNickname);
        final MyProfileActivity activity = (MyProfileActivity) getActivity();
        new ProfileRequest(getContext()).updateProfile(profileJson, new CustomRequest.ApiResponse<ProfileUpdate>() {
            @Override
            public void onSuccess(ProfileUpdate data) {
                if (data.getErrors() == null) {
                    UserHelper.setPushNotificationsEnabled(receivePushNotifications);
                    UserHelper.setUsername(newNickname);
                    if (activity != null) {
                        activity.profileFragment.updateView();
                    }
                    setUpUsername();
                    Log.e(TAG, "profile update success");
                } else {
                    //show toast that note write has failed
                    Log.e(TAG, "profile update  failed");
                }
            }
            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }
}
