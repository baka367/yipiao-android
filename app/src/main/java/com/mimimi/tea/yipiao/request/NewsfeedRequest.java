package com.mimimi.tea.yipiao.request;

import android.content.Context;

import com.mimimi.tea.yipiao.YipiaoApplication;
import com.mimimi.tea.yipiao.model.BaseModel;
import com.mimimi.tea.yipiao.model.newsfeed.NewsfeedTea;
import com.mimimi.tea.yipiao.model.newsfeed.NewsfeedTeaList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class NewsfeedRequest extends CustomRequest {
    private ApiInterface api;

    public NewsfeedRequest(Context context) {
        super(context);
        api = YipiaoApplication.restAdapter.create(ApiInterface.class);
    }


    private interface ApiInterface {
        @GET("/api/newsfeed/today")
        Call<BaseModel<NewsfeedTea>> getNewsfeedToday();

        @GET("/api/newsfeed/get/{id}")
        Call<BaseModel<NewsfeedTea>> getNewsfeedTea(@Path("id") int newsfeedTeaId);

        @GET("/api/newsfeed/list/{page}")
        Call<BaseModel<NewsfeedTeaList>> getNewsfeedTeaList(@Path("page") int page, @Query("limit") int limit);
    }

    public CustomRequest getNewsfeedToday(final ApiResponse<NewsfeedTea> listener) {
        completeContentCall(api.getNewsfeedToday(), listener);
        return this;
    }

    public CustomRequest getNewsfeedTea(int id, final ApiResponse<NewsfeedTea> listener) {
        completeContentCall(api.getNewsfeedTea(id), listener);
        return this;
    }

    public CustomRequest getNewsfeedList(int page, int limit, final ApiResponse<NewsfeedTeaList> listener) {
        completeContentCall(api.getNewsfeedTeaList(page, limit), listener);
        return this;
    }
}
