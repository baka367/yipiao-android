package com.mimimi.tea.yipiao.model.tea;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mimimi.tea.yipiao.model.style.Font;

import java.io.Serializable;
import java.util.List;

public class Tea implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tea_type")
    @Expose
    private String teaType;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("is_favourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("description")
    @Expose
    private List<Description> description = null;
    @SerializedName("font")
    @Expose
    private List<Font> font = null;
    @SerializedName("note")
    @Expose
    private Note note;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeaType() {
        return teaType;
    }

    public void setTeaType(String teaType) {
        this.teaType = teaType;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public List<Description> getDescription() {
        return description;
    }

    public void setDescription(List<Description> description) {
        this.description = description;
    }

    public List<Font> getFont() {
        return font;
    }

    public void setFont(List<Font> font) {
        this.font = font;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}