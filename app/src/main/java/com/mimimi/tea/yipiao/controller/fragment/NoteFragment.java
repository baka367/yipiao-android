package com.mimimi.tea.yipiao.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.emptyResponse.NoteWrite;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.NoteRequest;
import com.mimimi.tea.yipiao.view.TopBar;

import java.util.HashMap;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NoteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NoteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NoteFragment extends SupportBlurDialogFragment {
    private static final String TAG = "Note write activity";
    private static final String ARG_TEA_ID = "teaId";
    private static final String ARG_TEA_NAME = "teaName";
    private static final String ARG_NOTE_TEXT = "noteText";

    private int teaId;
    private String teaName;
    private String noteText;

    private EditText teaNoteET;

    private OnFragmentInteractionListener mListener;

    public NoteFragment() {
    }

    public interface OnNoteUpdateListener {
        void updateNote(String updatedNote);
    }

    OnNoteUpdateListener noteUpdateListener;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param teaId Tea Id.
     * @return A new instance of fragment NoteFragment.
     */
    public static NoteFragment newInstance(int teaId, String teaName, String noteText) {
        NoteFragment fragment = new NoteFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TEA_ID, teaId);
        args.putString(ARG_TEA_NAME, teaName);
        args.putString(ARG_NOTE_TEXT, noteText);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            teaId = getArguments().getInt(ARG_TEA_ID);
            teaName = getArguments().getString(ARG_TEA_NAME);
            noteText = getArguments().getString(ARG_NOTE_TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        LinearLayout rootLayout = (LinearLayout) inflater.inflate(R.layout.fragment_note, container, false);

        TopBar topBar = new TopBar(getContext(), TopBar.LEFT_BUTTON_NONE, TopBar.RIGHT_BUTTON_NONE, TopBar.MIDDLE_BUTTON_BACK);
        rootLayout.addView(topBar, 0);

        TextView teaNameTW = (TextView) rootLayout.findViewById(R.id.note_tea_name);
        teaNameTW.setText(teaName);
        teaNameTW.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_SONGTI_SC_REGULAR));

        teaNoteET = (EditText) rootLayout.findViewById(R.id.note_text);
        teaNoteET.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_MEDIUM));
        teaNoteET.setText(noteText);
        teaNoteET.setImeOptions(EditorInfo.IME_ACTION_DONE);

        teaNoteET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                teaNoteET.setCursorVisible(true);
            }
        });

        TextView sendToStaffButton = (TextView) rootLayout.findViewById(R.id.send_to_staff_button);
        sendToStaffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(true);
            }
        });

        TextView saveNoteButton = (TextView) rootLayout.findViewById(R.id.save_note_button);
        saveNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(false);
            }
        });

        return rootLayout;
    }

    public void sendMessage(boolean notifyStaff) {
        HashMap<String, Object> noteJson = new HashMap<>();
        HashMap<String, Object> tea = new HashMap<>();
        tea.put("id", teaId);
        noteJson.put("note", teaNoteET.getText().toString());
        noteJson.put("tea", tea);
        noteJson.put("notify_staff", notifyStaff);
        new NoteRequest(getContext()).writeNote(noteJson, new CustomRequest.ApiResponse<NoteWrite>() {
            @Override
            public void onSuccess(NoteWrite data) {
                if (data == null) {
                    noteUpdateListener.updateNote(teaNoteET.getText().toString());
                    ((Activity) getContext()).onBackPressed();
                } else {
                    //show toast that note write has failed
                    Log.e(TAG, "note write failed");
                }
            }
            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        noteUpdateListener = (OnNoteUpdateListener) context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onDestroyView() {
        dismiss();
        super.onDestroyView();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    protected float getDownScaleFactor() {
        return 7.0f;
    }
}
