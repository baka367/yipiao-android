package com.mimimi.tea.yipiao.model.tea;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Vilkazz on 2018-01-08.
 */

public class TeaList {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("teas")
    @Expose
    private List<TeaListItem> teas = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<TeaListItem> getTeas() {
        return teas;
    }

    public void setTeas(List<TeaListItem> teas) {
        this.teas = teas;
    }
}
