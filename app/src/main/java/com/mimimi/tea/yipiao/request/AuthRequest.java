package com.mimimi.tea.yipiao.request;

import android.content.Context;

import com.mimimi.tea.yipiao.YipiaoApplication;
import com.mimimi.tea.yipiao.model.BaseModel;
import com.mimimi.tea.yipiao.model.auth.AccessToken;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public class AuthRequest extends CustomRequest {

    private ApiInterface api;

    public AuthRequest(Context context) {
        super(context);
        api = YipiaoApplication.restAdapter.create(AuthRequest.ApiInterface.class);
    }

    private interface ApiInterface {
        @POST("/api/wechat/auth")
        @FormUrlEncoded
        Call<BaseModel<AccessToken>> doWeixinAuth(
                @Field("code") String code
        );

//        @POST("/2/auth/mobile-request-code")
//        @FormUrlEncoded
//        Call<MobileLogin> postMobileRequestCode(@Field("mobile_number") String mobileNumber,
//                                                @Field("password_reset") Integer passwordReset);
//
//        @POST("/2/auth/mobile-login")
//        @FormUrlEncoded
//        Call<MobileLogin> postMobileLogin(@Field("mobile_number") String mobileNumber,
//                                          @Field("confirmation_code") String confirmationCode,
//                                          @Field("login_access_key") String loginAccessKey);
//
//        @POST("/2/auth/mobile-confirm-code")
//        @FormUrlEncoded
//        Call<MobileLogin> postMobileConfirmCode(@Field("mobile_number") String mobileNumber,
//                                                @Field("confirmation_code") String confirmationCode,
//                                                @Field("login_access_key") String loginAccessKey);
//
//        @POST("/2/auth/mobile-register")
//        @FormUrlEncoded
//        Call<MobileLogin> postMobileRegister(@Field("mobile_number") String mobileNumber,
//                                             @Field("login_access_key") String loginAccessKey,
//                                             @Field("registration_key") String registrationKey,
//                                             @Field("password") String password);
//
//        @POST("/2/auth/mobile-reset-password")
//        @FormUrlEncoded
//        Call<MobileLogin> postMobilePasswordReset(@Field("mobile_number") String mobileNumber,
//                                                  @Field("login_access_key") String loginAccessKey,
//                                                  @Field("password_reset_key") String passwordResetKey,
//                                                  @Field("password") String password);
//
//        @POST("/2/auth/login")
//        @FormUrlEncoded
//        Call<MobileLoginWithPassword> postMobileLoginWithPassword(@Field("mobile_number") String mobileNumber,
//                                                                  @Field("password") String password);
    }

    public CustomRequest doWeixinAuth(String code, final ApiResponse<AccessToken> listener) {
        completeContentCall(api.doWeixinAuth(code), listener);
        return this;
    }


//    public CustomRequest postMobileRequestCode(String mobileNumber, Integer passwordReset, final MhbResponse<MobileLogin> listener) {
//        completeBareCall(api.postMobileRequestCode(mobileNumber, passwordReset), null, listener);
//        return this;
//    }
//
//    public CustomRequest postMobileLogin(String mobileNumber, String confirmationCode, String loginAccessKey, final MhbResponse<MobileLogin> listener) {
//        completeBareCall(api.postMobileLogin(mobileNumber, confirmationCode, loginAccessKey), null, listener);
//        return this;
//    }
//
//    public CustomRequest postMobileConfirmCode(String mobileNumber, String confirmationCode, String loginAccessKey, final MhbResponse<MobileLogin> listener) {
//        completeBareCall(api.postMobileConfirmCode(mobileNumber, confirmationCode, loginAccessKey), null, listener);
//        return this;
//    }
//
//    public CustomRequest postMobileRegister(String mobileNumber, String loginAccessKey, String registrationKey, String password, final MhbResponse<MobileLogin> listener) {
//        completeBareCall(api.postMobileRegister(mobileNumber, loginAccessKey, registrationKey, password), null, listener);
//        return this;
//    }
//
//    public CustomRequest postMobilePasswordReset(String mobileNumber, String loginAccessKey, String passwordResetKey, String password, final MhbResponse<MobileLogin> listener) {
//        completeBareCall(api.postMobilePasswordReset(mobileNumber, loginAccessKey, passwordResetKey, password), null, listener);
//        return this;
//    }
//
//    public CustomRequest postMobileLoginWithPassword(String mobileNumber, String password, final MhbResponse<MobileLoginWithPassword> listener) {
//        completeBareCall(api.postMobileLoginWithPassword(mobileNumber, password), null, listener);
//        return this;
//    }

}
