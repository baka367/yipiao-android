package com.mimimi.tea.yipiao.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.TeaDetailsItemDecoration.TeaDetailsItemDecoration;
import com.mimimi.tea.yipiao.adapter.TeaDetailsRVAdapter;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.helper.LogHelper;
import com.mimimi.tea.yipiao.helper.ShareHelper;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.tea.Description;
import com.mimimi.tea.yipiao.model.tea.DescriptionItem;
import com.mimimi.tea.yipiao.model.tea.Tea;
import com.mimimi.tea.yipiao.model.tea.TeaLike;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.UserRequest;
import com.mimimi.tea.yipiao.view.TopBar;

import java.util.ArrayList;
import java.util.List;

public class TeaDetailsFragment extends Fragment{
    private static final String TAG = "Tea Details Fragment";

    private RelativeLayout rootLayout;
    private RelativeLayout buttonNoteHolder;
    private ImageView buttonFavourite;
    RelativeLayout buttonFavouriteHolder;
    private RecyclerView teaDetails;
    private TopBar topBar;
    AbstractActivity activity;
    TeaDetailsFragment ateaDetailsFragment;
    OnTeaDetailsAnimationListener teaDetailsAnimationListener;
    LinearLayoutManager linearLayoutManager;

    private boolean teaLiked = false;
    private boolean showFragmentBackground;
    private boolean isExiting = false;
    private Tea tea;
    List<DescriptionItem> details;
    TeaDetailsRVAdapter teaDetailsAdapter;

    int positionHistory = 0;
    int positionWater = 0;
    int positionStorage = 0;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_tea_details, container, false);

        //prevent clickthrough
        rootLayout.setClickable(true);

        activity = ((AbstractActivity) getActivity());
        ateaDetailsFragment = this;;
        linearLayoutManager = new LinearLayoutManager(getContext());
        teaDetailsAnimationListener = (OnTeaDetailsAnimationListener) getContext();

        createView();
        addTopbar();
        return rootLayout;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        showDetails(tea, showFragmentBackground);  //HERE we setup the view
        showTextFadeIn();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void addTopbar() {
        topBar = new TopBar(getContext(), TopBar.LEFT_BUTTON_PROFILE, TopBar.RIGHT_BUTTON_SEARCH, TopBar.MIDDLE_BUTTON_BACK);
        rootLayout.addView(topBar, 0);
        topBar.bringToFront();
        topBar.invalidate();
//        Handler topBarHandler = new Handler();
//        topBarHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                rootLayout.addView(topBar, 0);
//                topBar.bringToFront();
//                topBar.invalidate();
//            }
//        },300);

    }

    private void createRecyclerView(Context context) {
        teaDetails = new RecyclerView(context);
    }

    private void createView() {
        RelativeLayout bottomButtonHolder = new RelativeLayout(getContext());
        bottomButtonHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams bottomButtonHolderParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        bottomButtonHolderParams.addRule(RelativeLayout.CENTER_VERTICAL);
        bottomButtonHolderParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        bottomButtonHolderParams.setMargins(
                (int) getResources().getDimension(R.dimen.padding15),
                0,
                (int) getResources().getDimension(R.dimen.padding15),
                (int) getResources().getDimension(R.dimen.padding11));
        bottomButtonHolder.setLayoutParams(bottomButtonHolderParams);

        buttonFavourite = new ImageView(getContext());
        buttonFavourite.setImageResource(R.drawable.ico_bglight_list_like_active);
        buttonFavouriteHolder = new RelativeLayout(getContext());
        buttonFavouriteHolder.setId(View.generateViewId());
        buttonFavouriteHolder.addView(buttonFavourite);
        RelativeLayout.LayoutParams buttonFavouriteParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.standard_button_dimen),
                (int) getResources().getDimension(R.dimen.standard_button_dimen)
        );
        buttonFavouriteParams.addRule(RelativeLayout.CENTER_VERTICAL);
        buttonFavouriteParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        buttonFavouriteHolder.setLayoutParams(buttonFavouriteParams);
        bottomButtonHolder.addView(buttonFavouriteHolder);

        ImageView buttonNote = new ImageView(getContext());
        buttonNote.setImageResource(R.drawable.ico_bglight_topbar_write);
        buttonNoteHolder = new RelativeLayout(getContext());
        buttonNoteHolder.setId(View.generateViewId());
        buttonNoteHolder.addView(buttonNote);
        RelativeLayout.LayoutParams buttonNoteParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.standard_button_dimen),
                (int) getResources().getDimension(R.dimen.standard_button_dimen)
        );
        buttonNoteParams.addRule(RelativeLayout.CENTER_VERTICAL);
        buttonNoteParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        buttonNoteHolder.setLayoutParams(buttonNoteParams);
        bottomButtonHolder.addView(buttonNoteHolder);

        rootLayout.addView(bottomButtonHolder);

        RelativeLayout rightSideButtonHolder = new RelativeLayout(getContext());
        rightSideButtonHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams rightSideButtonHolderParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        rightSideButtonHolderParams.addRule(RelativeLayout.CENTER_VERTICAL);
        rightSideButtonHolderParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        rightSideButtonHolderParams.setMarginEnd((int) getResources().getDimension(R.dimen.padding15));
        rightSideButtonHolder.setLayoutParams(rightSideButtonHolderParams);

        RelativeLayout.LayoutParams rightSideButtonTwParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rightSideButtonTwParams.addRule(RelativeLayout.CENTER_IN_PARENT, 1);


        final TextView buttonHistory = new TextView(getContext());
        buttonHistory.setText("史");
        buttonHistory.setTextColor(getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
        buttonHistory.setGravity(Gravity.CENTER);
        buttonHistory.setLayoutParams(rightSideButtonTwParams);
        final RelativeLayout buttonHistoryHolder = new RelativeLayout(getContext());
        buttonHistoryHolder.setBackground(getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));

        buttonHistoryHolder.setId(View.generateViewId());
        buttonHistoryHolder.addView(buttonHistory);
        RelativeLayout.LayoutParams buttonHistoryParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.small_button_dimen),
                (int) getResources().getDimension(R.dimen.small_button_dimen)
        );
        buttonHistoryParams.addRule(RelativeLayout.CENTER_VERTICAL);
        buttonHistoryParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        buttonHistoryHolder.setLayoutParams(buttonHistoryParams);
        rightSideButtonHolder.addView(buttonHistoryHolder);

        final TextView buttonWater = new TextView(getContext());
        buttonWater.setText("水");
        buttonWater.setTextColor(getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
        buttonWater.setGravity(Gravity.CENTER);
        buttonWater.setLayoutParams(rightSideButtonTwParams);
        final RelativeLayout buttonWaterHolder = new RelativeLayout(getContext());
        buttonWaterHolder.setBackground(getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
        buttonWaterHolder.setId(View.generateViewId());
        buttonWaterHolder.addView(buttonWater);
        RelativeLayout.LayoutParams buttonWaterParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.small_button_dimen),
                (int) getResources().getDimension(R.dimen.small_button_dimen)
        );
        buttonWaterParams.setMargins(0, (int) getResources().getDimension(R.dimen.padding20), 0, 0);
        buttonWaterParams.addRule(RelativeLayout.CENTER_VERTICAL);
        buttonWaterParams.addRule(RelativeLayout.BELOW, buttonHistoryHolder.getId());
        buttonWaterHolder.setLayoutParams(buttonWaterParams);
        rightSideButtonHolder.addView(buttonWaterHolder);

        final TextView buttonStorage = new TextView(getContext());
        buttonStorage.setText("器");
        buttonStorage.setTextColor(getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
        buttonStorage.setGravity(Gravity.CENTER);
        buttonStorage.setLayoutParams(rightSideButtonTwParams);
        final RelativeLayout buttonStorageHolder = new RelativeLayout(getContext());
        buttonStorageHolder.setBackground(getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
        buttonStorageHolder.setId(View.generateViewId());
        buttonStorageHolder.addView(buttonStorage);
        RelativeLayout.LayoutParams buttonStorageParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.small_button_dimen),
                (int) getResources().getDimension(R.dimen.small_button_dimen)
        );
        buttonStorageParams.addRule(RelativeLayout.CENTER_VERTICAL);
        buttonStorageParams.addRule(RelativeLayout.BELOW, buttonWaterHolder.getId());
        buttonStorageParams.setMargins(0, (int) getResources().getDimension(R.dimen.padding20), 0, 0);
        buttonStorageHolder.setLayoutParams(buttonStorageParams);
        rightSideButtonHolder.addView(buttonStorageHolder);

        ImageView buttonShare = new ImageView(getContext());
        buttonShare.setImageResource(R.drawable.ico_bglight_list_sidetab_share);
        RelativeLayout buttonShareHolder = new RelativeLayout(getContext());
        buttonShareHolder.setId(View.generateViewId());
        buttonShareHolder.addView(buttonShare);
        RelativeLayout.LayoutParams buttonShareParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.share_button_dimen),
                (int) getResources().getDimension(R.dimen.share_button_dimen)
        );
        buttonShareParams.addRule(RelativeLayout.CENTER_VERTICAL);
        buttonShareParams.addRule(RelativeLayout.BELOW, buttonStorageHolder.getId());
        buttonShareParams.setMargins((int) getResources().getDimension(R.dimen.padding2), (int) getResources().getDimension(R.dimen.padding20), 0, 0);
        buttonShareHolder.setLayoutParams(buttonShareParams);
        rightSideButtonHolder.addView(buttonShareHolder);

        rootLayout.addView(rightSideButtonHolder);

        teaDetails = new RecyclerView(getContext());
        teaDetails.setFadingEdgeLength(100);
        teaDetails.setVerticalScrollBarEnabled(false);
        teaDetails.setVerticalFadingEdgeEnabled(true);
        teaDetails.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        teaDetails.setLayoutManager(linearLayoutManager);
        teaDetails.setHasFixedSize(true);
        teaDetails.addItemDecoration(new TeaDetailsItemDecoration(getContext()));
        teaDetails.setOverScrollMode(View.OVER_SCROLL_NEVER);
        teaDetails.setPadding((int) getResources().getDimension(R.dimen.padding36), (int) getResources().getDimension(R.dimen.padding60), (int) getResources().getDimension(R.dimen.padding36), 0);

        RelativeLayout listViewHolder = new RelativeLayout(getContext());
        listViewHolder.setBackgroundColor(getResources().getColor(R.color.colorPureWhite));
        RelativeLayout.LayoutParams listViewHolderParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        listViewHolderParams.setMargins(
                (int) getResources().getDimension(R.dimen.padding28), 0,
                (int) getResources().getDimension(R.dimen.padding28), 0);
        listViewHolderParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        listViewHolder.setLayoutParams(listViewHolderParams);
        listViewHolder.addView(teaDetails);

        UtilHelper.expandTouchArea(rootLayout, buttonHistoryHolder);
        buttonHistoryHolder.setOnClickListener(buttonHistoryOnClickListener);
        UtilHelper.expandTouchArea(rootLayout, buttonWaterHolder);
        buttonWaterHolder.setOnClickListener(buttonWaterOnClickListener);
        UtilHelper.expandTouchArea(rootLayout, buttonStorageHolder);
        buttonStorageHolder.setOnClickListener(buttonStorageOnClickListener);
        UtilHelper.expandTouchArea(rootLayout, buttonShareHolder);
        buttonShareHolder.setOnClickListener(buttonShareOnClickListener);
        UtilHelper.expandTouchArea(rootLayout, buttonFavouriteHolder);

        teaDetails.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView view, int scrollState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                if ( firstVisiblePosition <= positionHistory) {
                    buttonHistoryHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar_s, null));
                    buttonWaterHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
                    buttonStorageHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
                    buttonHistory.setTextColor(activity.getResources().getColor(R.color.colorPureWhite));
                    buttonWater.setTextColor(activity.getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
                    buttonStorage.setTextColor(activity.getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
                } else if (firstVisiblePosition > positionHistory && firstVisiblePosition <= positionWater) {
                    buttonHistoryHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
                    buttonWaterHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar_s, null));
                    buttonStorageHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
                    buttonHistory.setTextColor(activity.getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
                    buttonWater.setTextColor(activity.getResources().getColor(R.color.colorPureWhite));
                    buttonStorage.setTextColor(activity.getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
                } else if (firstVisiblePosition > positionWater) {
                    buttonHistoryHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
                    buttonWaterHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar, null));
                    buttonStorageHolder.setBackground(activity.getResources().getDrawable(R.drawable.ico_tea_details_right_bar_s, null));
                    buttonHistory.setTextColor(activity.getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
                    buttonWater.setTextColor(activity.getResources().getColorStateList(R.color.tea_details_right_side_button_text_color));
                    buttonStorage.setTextColor(activity.getResources().getColor(R.color.colorPureWhite));
                }
            }
                   });

        rootLayout.addView(listViewHolder);
        //bring buttons to front of the view
        rightSideButtonHolder.bringToFront();
        rightSideButtonHolder.invalidate();
        bottomButtonHolder.bringToFront();
        bottomButtonHolder.invalidate();
    }

    private void showDetails(final Tea tea, final boolean showBackground) {
        setUpBackground(showBackground);
        teaLiked = tea.getIsFavourite();

        setFavouriteButton(teaLiked);
        long startTime = System.nanoTime();
        teaDetails.setAdapter(teaDetailsAdapter);
        long midTime = System.nanoTime();
        LogHelper.getInstance().logEvent(TAG, "crating adapter took: " + (midTime - startTime));
//        teaDetails.getAdapter().notifyDataSetChanged();

        View.OnClickListener buttonFavouriteOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo guest path disabled for now
                if (!UserHelper.requireLogin(getActivity(),false)) {
                    //change tea like status
                    toggleTeaLike();
                    new UserRequest(getContext()).likeTea(tea.getId(), new CustomRequest.ApiResponse<TeaLike>() {
                        @Override
                        public void onSuccess(TeaLike data) {
                            if (data != null) {
                                //reverse tea like if response is not normal
                                toggleTeaLike();
                            } else {
                                Log.e(TAG, "liked tea");
                            }
                        }

                        @Override
                        public void onFail(String message) {
                            //reverse tea like if request fails
                            toggleTeaLike();
                            Log.e(TAG, message);
                        }
                    });
                }
            }
        };

        buttonFavouriteHolder.setOnClickListener(buttonFavouriteOnClickListener);

        buttonNoteHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showNoteFragment(tea);
            }
        });
    }

    private View.OnClickListener buttonHistoryOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            linearLayoutManager.scrollToPositionWithOffset(0, 20);
        }
    };
    private View.OnClickListener buttonWaterOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            linearLayoutManager.scrollToPositionWithOffset(positionWater, 20);
        }
    };
    private View.OnClickListener buttonStorageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            linearLayoutManager.scrollToPositionWithOffset(positionStorage, 20);
        }
    };
    private View.OnClickListener buttonShareOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ShareHelper.showShareFragment(getContext(), ShareHelper.SHARE_TYPE_TEA, tea);
        }
    };

    private void toggleTeaLike() {
        teaLiked = !teaLiked;
        setFavouriteButton(teaLiked);
    }

    private void setFavouriteButton(boolean state) {
        if (buttonFavourite == null) return;
        if(state) {
            buttonFavourite.setImageResource(R.drawable.ico_bglight_list_like_active);
        } else {
            buttonFavourite.setImageResource(R.drawable.ico_bglight_list_like_inactive);
        }
    }

    public void setTea(Tea tea, boolean showBackground, Context context) {
        this.tea = tea;

        details = new ArrayList<>();

        for (Description description : tea.getDescription()) {
            switch (description.getType()) {
                case "history":
                    positionHistory = details.size();
                    break;
                case "water":
                    positionWater = details.size();
                    break;
                case "storage":
                    positionStorage = details.size();
                    break;
                default:
                    break;
            }
            details.addAll(description.getItems());
        }

        if (teaDetailsAdapter == null) {
            teaDetailsAdapter = new TeaDetailsRVAdapter(context, details);
        }
        teaDetailsAdapter.notifyDataSetChanged();
        this.showFragmentBackground = showBackground;
    }

    public void showTextFadeIn() {
        final Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(1000);
        teaDetails.setAnimation(fadeIn);
    }

    public void setUpBackground(boolean show) {
        if (show) {
            UtilHelper.setBackgroundImage(getContext(), rootLayout, this.tea.getCoverImage());
        }
    }

    public void setExiting() {
        isExiting = true;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {

        Animation anim = AnimationUtils.loadAnimation(activity, nextAnim);

        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                LogHelper.getInstance().logEvent(TAG, "Animation started.");
                teaDetailsAnimationListener.updateViewOnTeaDetailsAnimation(isExiting);
                if (isExiting) {
                    isExiting = false;
                }
                // additional functionality
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                LogHelper.getInstance().logEvent(TAG, "Animation repeating.");
                // additional functionality
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                LogHelper.getInstance().logEvent(TAG, "Animation ended.");
//                teaDetails.setAdapter(teaDetailsAdapter);
//                teaDetailsAdapter.notifyDataSetChanged();
                // additional functionality

            }
        });

        return anim;
    }
    public interface OnTeaDetailsAnimationListener {
        void updateViewOnTeaDetailsAnimation(boolean isExiting);
    }
}
