package com.mimimi.tea.yipiao.controller.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.helper.ShareHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;

import java.io.IOException;
import java.util.HashMap;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

public class ShareFragment extends SupportBlurDialogFragment {
    private static final String TAG = "Note write activity";
    private static final String ARG_SHARE_TYPE = "shareType";
    private static final String ARG_SHARE_DATA = "shareData";

    private HashMap <String, String> shareBlockData;
    private int shareType;
    RelativeLayout rootLayout;
    private Bitmap shareImage = null;

    private ShareFragment.OnFragmentInteractionListener mListener;
    private Activity activity;

    public ShareFragment() {
    }

    public interface onShareListener {
        public void logShare();
    }

    ShareFragment.onShareListener onShareListener;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NoteFragment.
     */
    public static <T> ShareFragment newInstance(int shareType, HashMap shareObject) {
        ShareFragment fragment = new ShareFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SHARE_TYPE, shareType);
       ;
        if (shareObject != null) {
            args.putSerializable(ARG_SHARE_DATA, shareObject);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = ((Activity) getContext());
        if (getArguments() != null) {
            shareType = getArguments().getInt(ARG_SHARE_TYPE);
            shareBlockData = (HashMap<String, String>)getArguments().getSerializable(ARG_SHARE_DATA);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        rootLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_share, container, false);

        if (shareBlockData != null && shareBlockData.get("image") != null) {
            prepareShareImage(rootLayout);
        }
        initButtons(rootLayout);
        rootLayout.setClickable(true);

        return rootLayout;
    }

    private void prepareShareImage(final RelativeLayout rootLayout) {
        Glide
                .with(getContext())
                .load(Constants.IMAGE_STORE_BASE_URL + shareBlockData.get("image") + UtilHelper.getOnlyWidthCroppedImage(100))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        shareImage = resource;
                        initButtons(rootLayout);
                    }
                });

    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        dismiss();
        super.onDestroyView();
    }


    public void initButtons(RelativeLayout rootLayout) {
        RelativeLayout shareWeixinBtn = rootLayout.findViewById(R.id.share_weixin_btn);
        shareWeixinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.weixinLink(getContext(), shareType, shareBlockData, false, shareImage);
                activity.onBackPressed();
            }
        });
        RelativeLayout shareMomentsBtn = rootLayout.findViewById(R.id.share_moments_btn);
        shareMomentsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.weixinLink(getContext(), shareType, shareBlockData, true, shareImage);
                activity.onBackPressed();
            }
        });
        RelativeLayout shareLinkBtn = rootLayout.findViewById(R.id.share_link_btn);
        shareLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.plainLink(getContext(), shareType, shareBlockData);
                activity.onBackPressed();
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    protected float getDownScaleFactor() {
        return 14.0f;
    }
}
