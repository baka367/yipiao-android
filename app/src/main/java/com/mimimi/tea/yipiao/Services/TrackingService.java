package com.mimimi.tea.yipiao.Services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mimimi.tea.yipiao.helper.LogHelper;
import com.mimimi.tea.yipiao.model.emptyResponse.Tracking;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.TrackingRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TrackingService extends Service {;
    public static String TAG = "TRACKING SERVICE";

    public static final int TYPE_EXPOSURE = 0;
    public static final int TYPE_ENGAGEMENT = 1;


    public static final int ENG_TYPE_TEA_NEWSFEED = 0;
    public static final int ENG_TYPE_TEA_PROFILE = 3;
    public static final int ENG_TYPE_SHARE_APP = 14;
    public static final int ENG_TYPE_SHARE_TEA = 15;
    public static final int ENG_TYPE_NEWSFEED_SWIPE = 20;

    List<HashMap<String , Object>> dataFootprints = new ArrayList<>();


    // Binder given to clients
    private final IBinder mBinder = new TrackingBinder();

    public void addDataFootprint(int dataType, int eventType, HashMap<String, Integer> args){
        HashMap<String, Object> dataFootprint = new HashMap<>();
        dataFootprint.put("data_type", dataType);
        dataFootprint.put("event_type", eventType);
        if (args != null) {
            dataFootprint.putAll(args);
        }
        dataFootprints.add(dataFootprint);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class TrackingBinder extends Binder {
        public TrackingService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TrackingService.this;
        }
    }

    private Timer timer = new Timer();

    @Override
    public void onCreate() {
        super.onCreate();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
               sendTrackingData();
            }
        }, 0, 30000);//30 Seconds

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shutdownService();

    }

    public void shutdownService() {
        sendTrackingData();
    }

    public void sendTrackingData() {
        if (dataFootprints.size() == 0) return;
        LogHelper.getInstance().logEvent(TAG, "SENDING DATA ");
        HashMap<String, Object> dataWrapper = new HashMap<>();
        dataWrapper.put("data", dataFootprints);
        new TrackingRequest(this).tracking(dataWrapper, new CustomRequest.ApiResponse<Tracking>() {
            @Override
            public void onSuccess(Tracking data) {
                dataFootprints.clear();
            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }
}
