package com.mimimi.tea.yipiao.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.tea.DescriptionItem;

import java.util.List;

import okhttp3.internal.Util;

/**
 * Created by Vilkazz on 2018-03-18.
 */

public class TeaDetailsRVAdapter extends RecyclerView.Adapter {
    private List<DescriptionItem> descriptionItems;
    private Context context;



    public TeaDetailsRVAdapter(Context context, List<DescriptionItem> items) {
        descriptionItems = items;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View headerView;
        switch (viewType) {
            case DescriptionItem.TYPE_TEXT:
                headerView = inflater.inflate(R.layout.tea_details_text_layout, parent, false);
                viewHolder = new TeaTextItemHolder(headerView, context);
                break;
            case DescriptionItem.TYPE_TITLE:
                headerView = inflater.inflate(R.layout.tea_details_title_layout, parent, false);
                viewHolder = new TeaTitleItemHolder(headerView, context);
                break;
            case DescriptionItem.TYPE_MAIN_TITLE:
                headerView = inflater.inflate(R.layout.tea_details_title_layout, parent, false);
                viewHolder = new TeaMainTitleItemHolder(headerView, context);
                break;
            case DescriptionItem.TYPE_IMAGE:
                headerView = inflater.inflate(R.layout.tea_details_image_layout, parent, false);
                viewHolder = new TeaImageItemHolder(headerView, context);
                break;
            default:
                headerView = inflater.inflate(R.layout.tea_details_text_layout, parent, false);
                viewHolder = new TeaTextItemHolder(headerView, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DescriptionItem descriptionItem = descriptionItems.get(position);
        switch (holder.getItemViewType()) {
             case DescriptionItem.TYPE_TEXT:
                ((TeaTextItemHolder) holder).teaText.setText(descriptionItem.getValue());
                 break;
             case DescriptionItem.TYPE_TITLE:
                 ((TeaTitleItemHolder) holder).teaTitle.setText(descriptionItem.getValue());
                 break;
             case DescriptionItem.TYPE_MAIN_TITLE:
                 ((TeaMainTitleItemHolder) holder).teaTitle.setText(descriptionItem.getValue());
                 break;
             case DescriptionItem.TYPE_IMAGE:
                 UtilHelper.setImage(context, ((TeaImageItemHolder)holder).teaImage, descriptionItem.getValue());
                 break;
             default:
                 ((TeaTextItemHolder) holder).teaText.setText(descriptionItem.getValue());
                 break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return descriptionItems.get(position).getTypeId();
    }

    @Override
    public int getItemCount() {
        return descriptionItems.size();
    }

    private static class TeaTextItemHolder extends ViewHolder {
        TextView teaText;

        TeaTextItemHolder(View itemView, Context context) {
            super(itemView);
            teaText = itemView.findViewById(R.id.tea_details_text_view);
            teaText.setTypeface(UtilHelper.getFont(context, Constants.FONT_PING_FANG_LIGHT));
            teaText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_15));
        }
    }

    private static class TeaTitleItemHolder extends ViewHolder {
        TextView teaTitle;

        TeaTitleItemHolder(View itemView, Context context) {
            super(itemView);
            teaTitle = itemView.findViewById(R.id.tea_details_text_title_view);
            teaTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_17));
            teaTitle.setTypeface(UtilHelper.getFont(context, Constants.FONT_PING_FANG_MEDIUM));
        }
    }

    private static class TeaMainTitleItemHolder extends ViewHolder {
        TextView teaTitle;

        TeaMainTitleItemHolder(View itemView, Context context) {
            super(itemView);
            teaTitle = itemView.findViewById(R.id.tea_details_text_title_view);
            teaTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_20));
            teaTitle.setTypeface(UtilHelper.getFont(context, Constants.FONT_SONGTI_SC_BOLD));
        }
    }

    private static class TeaImageItemHolder extends ViewHolder {
        ImageView teaImage;

        TeaImageItemHolder(View itemView, Context context) {
            super(itemView);
            teaImage = itemView.findViewById(R.id.tea_details_image_view);
        }
    }
}
