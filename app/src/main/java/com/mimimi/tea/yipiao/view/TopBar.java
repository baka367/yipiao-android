package com.mimimi.tea.yipiao.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.controller.activity.LoginActivity;
import com.mimimi.tea.yipiao.controller.activity.MyProfileActivity;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.User;

import java.util.Objects;


public class TopBar extends RelativeLayout {
    public static final int LEFT_BUTTON_NONE = 0;
    public static final int LEFT_BUTTON_PROFILE = 1;
    public static final int LEFT_BUTTON_BACK_ACTIVITY = 2;
    public static final int LEFT_BUTTON_BACK_FRAGMENT = 3;

    public static final int RIGHT_BUTTON_NONE = 0;
    public static final int RIGHT_BUTTON_SEARCH = 1;
    public static final int RIGHT_BUTTON_SETTINGS = 2;

    public static final int MIDDLE_BUTTON_NONE = 0;
    public static final int MIDDLE_BUTTON_BACK = 1;

    private static final int MODE_STANDARD = 0;
    private static final int MODE_SEARCH = 1;

    RelativeLayout topToolbarView;

    int[] standardModeButtons = new int[3];

    private final Context context;

    public TopBar(final Context context, int leftButton, int rightButton, int middleButton) {
        super(context);

        this.context = context;
        setId(View.generateViewId());
        topToolbarView = new RelativeLayout(context);

        RelativeLayout.LayoutParams mainLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.top_toolbar_height));
        mainLayoutParams.setMargins(0, (int) getResources().getDimension(R.dimen.padding11) + UtilHelper.getStatusBarHeight(context), 0, 0);
        topToolbarView.setLayoutParams(mainLayoutParams);

        standardModeButtons = new int[] {leftButton, rightButton, middleButton};

        setTopBarMode(MODE_STANDARD);
        addView(topToolbarView);
    }

    private void setTopBarMode(int mode) {
        switch (mode) {
            case MODE_STANDARD:
                setModeStandard();
                break;
            case MODE_SEARCH:
                setModeSearch();
                break;
        }
    }

    private void setModeStandard() {

        topToolbarView.removeAllViews();

        RelativeLayout leftButtonHolder = new RelativeLayout(context);

        leftButtonHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams leftButtonParams = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.top_toolbar_button),
                (int) getResources().getDimension(R.dimen.top_toolbar_button));
        leftButtonParams.addRule(RelativeLayout.CENTER_VERTICAL);
        leftButtonParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        leftButtonParams.setMarginStart((int) getResources().getDimension(R.dimen.padding15));
        leftButtonHolder.setLayoutParams(leftButtonParams);
        UtilHelper.expandTouchArea(this, leftButtonHolder);

        switch (standardModeButtons[0]) {
            case LEFT_BUTTON_NONE:
                leftButtonHolder.setVisibility(View.GONE);
                break;
            case LEFT_BUTTON_BACK_ACTIVITY:
                leftButtonHolder.setBackground(getResources().getDrawable(R.drawable.ico_bglight_list_goback));
                leftButtonHolder.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((AbstractActivity) context).finish();
                    }
                });
                break;
            case LEFT_BUTTON_BACK_FRAGMENT:
                leftButtonHolder.setBackground(getResources().getDrawable(R.drawable.ico_bglight_list_goback));
                leftButtonHolder.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((AbstractActivity) context).onBackPressed();
                    }
                });
                break;
            case LEFT_BUTTON_PROFILE:
                leftButtonHolder.setBackground(getResources().getDrawable(R.drawable.ico_bglight_topbar_user));
                leftButtonHolder.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((AbstractActivity) context).startActivity(new Intent(context, MyProfileActivity.class));
                    }
                });
                break;
            default:
                leftButtonHolder.setVisibility(View.GONE);
        }


        RelativeLayout rightButtonHolder = new RelativeLayout(context);
        rightButtonHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams rightButtonParams = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.top_toolbar_button),
                (int) getResources().getDimension(R.dimen.top_toolbar_button));
        rightButtonParams.addRule(RelativeLayout.CENTER_VERTICAL);
        rightButtonParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        rightButtonParams.setMarginEnd((int) getResources().getDimension(R.dimen.padding15));
        rightButtonHolder.setLayoutParams(rightButtonParams);
        UtilHelper.expandTouchArea(this, rightButtonHolder);

        switch (standardModeButtons[1]) {
            case RIGHT_BUTTON_NONE:
                rightButtonHolder.setVisibility(View.GONE);
                break;
            case RIGHT_BUTTON_SEARCH:
                rightButtonHolder.setBackground(getResources().getDrawable(R.drawable.ico_bglight_list_search_inactive));
                rightButtonHolder.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setTopBarMode(MODE_SEARCH);
                    }
                });
                break;
            case RIGHT_BUTTON_SETTINGS:
                rightButtonHolder.setBackground(getResources().getDrawable(R.drawable.ico_bglight_topbar_settings));
                rightButtonHolder.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (UserHelper.isGuest()) {
                            UserHelper.requireLogin((AbstractActivity) context,true);
                            ((AbstractActivity) context).finish();
                        } else {
                        if (Objects.equals(context.getClass().getName(), MyProfileActivity.class.getName())) {
                            ((MyProfileActivity) context).showSettingsFragment();
                        }
                    }
                    }
                });
                break;
            default:
                rightButtonHolder.setVisibility(View.GONE);
        }


        RelativeLayout middleButtonHolder = new RelativeLayout(context);
        middleButtonHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams middleButtonParams = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.top_toolbar_button),
                (int) getResources().getDimension(R.dimen.top_toolbar_button));
        middleButtonParams.addRule(RelativeLayout.CENTER_VERTICAL);
        middleButtonParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        middleButtonParams.setMarginEnd((int) getResources().getDimension(R.dimen.padding15));
        middleButtonHolder.setLayoutParams(middleButtonParams);
        UtilHelper.expandTouchArea(this, middleButtonHolder);

        switch (standardModeButtons[2]) {
            case MIDDLE_BUTTON_NONE:
                middleButtonHolder.setVisibility(View.GONE);
                break;
            case MIDDLE_BUTTON_BACK:
                middleButtonHolder.setBackground(getResources().getDrawable(R.drawable.ico_bglight_topbar_goback));
                middleButtonHolder.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((Activity) context).onBackPressed();
                    }
                });
                break;
            default:
                middleButtonHolder.setVisibility(View.GONE);
        }

        topToolbarView.addView(leftButtonHolder);
        topToolbarView.addView(rightButtonHolder);
        topToolbarView.addView(middleButtonHolder);
    }

    private void setModeSearch() {
        topToolbarView.removeAllViews();
        //todo change color to transparent
        topToolbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.tobBarBackground));

        int screenWidth = UtilHelper.getScreenWidth(context)
                - 2 * (int) getResources().getDimension(R.dimen.padding15)
                - (int) getResources().getDimension(R.dimen.padding8);

        RelativeLayout searchBarHolder = new RelativeLayout(context);

        searchBarHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams searchBarHolderParams = new RelativeLayout.LayoutParams(
                (int) (screenWidth * 0.91),
                ViewGroup.LayoutParams.MATCH_PARENT);
        searchBarHolderParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        searchBarHolderParams.setMarginStart((int) getResources().getDimension(R.dimen.padding15));
        searchBarHolder.setLayoutParams(searchBarHolderParams);

        final EditText searchBar = new EditText(context);
        searchBar.setId(View.generateViewId());
        searchBar.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        searchBar.setHint("");
        searchBar.setSingleLine();
        searchBar.setInputType(InputType.TYPE_CLASS_TEXT);
        searchBar.setTextColor(ContextCompat.getColor(context, R.color.colorYipiaoLightGold));
        searchBar.setFocusableInTouchMode(true);
        searchBar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_bglight_list_search, 0, 0, 0);
        searchBar.setBackground(getResources().getDrawable(R.drawable.shape_searchbar_rounded));
        searchBar.requestFocus();
        searchBar.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Toast.makeText(context, searchBar.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });

        searchBarHolder.addView(searchBar);


        RelativeLayout cancelButtonHolder = new RelativeLayout(context);
        cancelButtonHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams cancelButtonParams = new RelativeLayout.LayoutParams(
                (int) (screenWidth * 0.09),
                ViewGroup.LayoutParams.MATCH_PARENT);
        cancelButtonParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        cancelButtonParams.setMarginEnd((int) getResources().getDimension(R.dimen.padding15));
        cancelButtonHolder.setLayoutParams(cancelButtonParams);
        UtilHelper.expandTouchArea(this, cancelButtonHolder);

        TextView cancelButton = new TextView(context);
        cancelButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        cancelButton.setText(R.string.top_bar_search_cancel);
        cancelButton.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        cancelButtonHolder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setTopBarMode(MODE_STANDARD);
            }
        });
        cancelButtonHolder.addView(cancelButton);

        topToolbarView.addView(searchBarHolder);
        topToolbarView.addView(cancelButtonHolder);
    }
}
