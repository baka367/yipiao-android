package com.mimimi.tea.yipiao.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.controller.activity.NewsfeedActivity;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.newsfeed.NewsfeedTea;
import com.mimimi.tea.yipiao.model.tea.Tea;

public class NewsfeedFragment extends Fragment {

    NewsfeedActivity activity;
    Context context;

    private static final String TAG = "NewsfeedTea Fragment";

    private RelativeLayout rootLayout;
    private TextView teaName;
    private TextView teaType;
    private Tea tea;
    private NewsfeedTea aNewsfeed;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_newsfeed_tea, container, false);

        //prevent clickthrough
        rootLayout.setClickable(true);

        activity = ((NewsfeedActivity) getActivity());
        context = activity;

        createView();

        rootLayout.setRotationY(180);

        return rootLayout;
    }

    private void createView() {
        RelativeLayout teaNameHolder = new RelativeLayout(context);
        teaNameHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams teaNameHolderParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.text_title_view_width),
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        teaNameHolderParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        teaNameHolderParams.setMargins(0, (int) getResources().getDimension(R.dimen.padding13), (int) getResources().getDimension(R.dimen.padding15), 0);
        teaNameHolder.setLayoutParams(teaNameHolderParams);

        teaName = new TextView(context);
        teaName.setGravity(Gravity.CENTER);
        teaName.setWidth((int) getResources().getDimension(R.dimen.text_title_view_width));
        teaName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_24));
        teaName.setText("胖猫苹果");

        teaNameHolder.addView(teaName);

        RelativeLayout teaTypeHolder = new RelativeLayout(context);
        teaTypeHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams teaTypeHolderParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.text_tea_type_view_width),
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        teaTypeHolderParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        teaTypeHolderParams.setMargins((int) getResources().getDimension(R.dimen.padding15), (int) getResources().getDimension(R.dimen.padding15), 0, 0);
        teaTypeHolder.setLayoutParams(teaTypeHolderParams);
        teaType = new TextView(context);
        teaType.setGravity(Gravity.CENTER);
        teaType.setWidth((int) getResources().getDimension(R.dimen.text_tea_type_view_width));
        teaType.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_15));
        teaType.setText("中国茶");
        teaType.setTextColor(getResources().getColor(R.color.colorYipiaoGold));

        teaTypeHolder.addView(teaType);

        RelativeLayout teaDetailsHolder = new RelativeLayout(context);
        teaDetailsHolder.setId(View.generateViewId());
        RelativeLayout.LayoutParams teaDetailsHolderParams = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.news_feed_tea_details_width),
                (int) getResources().getDimension(R.dimen.news_feed_tea_details_height)
        );
        teaDetailsHolderParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        teaDetailsHolderParams.addRule(RelativeLayout.CENTER_VERTICAL);
        teaDetailsHolder.setLayoutParams(teaDetailsHolderParams);

        teaDetailsHolder.addView(teaNameHolder);
        teaDetailsHolder.addView(teaTypeHolder);
        teaDetailsHolder.setBackground(getResources().getDrawable(R.drawable.bg_newsfeed_text_field, null));

        rootLayout.addView(teaDetailsHolder);
    }

    public void setNewsfeed(NewsfeedTea newsfeed) {
        this.aNewsfeed = newsfeed;
    }

    /**
     * Parses NewsfeedPager response data and adds view to content
     */
    public void showTea() {
        if (aNewsfeed == null) return;
        this.tea = aNewsfeed.getTea();
        teaName.setText(this.tea.getName());
        teaType.setText(this.tea.getTeaType());
        UtilHelper.setBackgroundImage(context, getView(), tea.getCoverImage());
    }

    // newInstance constructor for creating fragment with arguments
    public static NewsfeedFragment newInstance() {
        return new NewsfeedFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Code executes EVERY TIME user views the fragment
        showTea();
    }
}
