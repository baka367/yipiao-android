package com.mimimi.tea.yipiao.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.helper.LogHelper;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

public class LoginActivity extends AbstractActivity {

    private static final String TAG = "Login Activity";

    private String requestedClass;
    private Bundle requestedExtras;
    private boolean activityWasClosed;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        UserHelper.registerLoginActivity(LoginActivity.this);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Log.e(TAG, "Login activity not initiated. Missing extras");
            finish();
        }
        requestedClass = extras.getString(Constants.LOGIN_REQUESTED_CLASS_NAME);
        requestedExtras = (Bundle) extras.get(Constants.LOGIN_REQUESTED_CLASS_BUNDLE);
        activityWasClosed = extras.getBoolean(Constants.KEY_ACTIVITY_WAS_CLOSED);
        setUpButtons();
    }

    private void setUpButtons() {
        TextView emailButton = (TextView)findViewById(R.id.login_email_button);
        TextView weixinButton = (TextView)findViewById(R.id.login_weixin_button);
        TextView cancelButton = (TextView)findViewById(R.id.login_cancel_button);

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "email login not found", Toast.LENGTH_LONG).show();
            }
        });

        weixinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IWXAPI api = WXAPIFactory.createWXAPI(getApplicationContext(), Constants.APP_ID);
                final SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "wechat_yipiao";
                api.sendReq(req);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void handleLoginResult(int result) {
//        UserHelper.CustomProgressDialog.finishLoading();

//        LogMhb.getInstance().logEvent(TAG, "handleLoginResult: " + result);
        if (result == Constants.RESULT_LOGIN_SUCCESS) {
            //open requested class with clean slate
            Class requestedActivityClass;
            try {
                requestedActivityClass = Class.forName(requestedClass);
            } catch (ClassNotFoundException ex) {
                requestedActivityClass = NewsfeedActivity.class;
                Toast.makeText(this, "Class error", Toast.LENGTH_SHORT).show();
            }
            if (activityWasClosed) {
                Intent intent = new Intent(this, requestedActivityClass);
                if (requestedExtras != null) {
                    intent.putExtras(requestedExtras);
                }
                startActivity(intent);
            }
            finish();
        } else {
            if (result == Constants.RESULT_LOGIN_CANCELED) {
//                Toast.makeText(this, R.string.wechat_login_cancelled, Toast.LENGTH_SHORT).show();
            }
            if (result == Constants.RESULT_LOGIN_ERROR) {
//                Toast.makeText(this, R.string.wechat_login_error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        UserHelper.registerLoginActivity(null);
    }
}


