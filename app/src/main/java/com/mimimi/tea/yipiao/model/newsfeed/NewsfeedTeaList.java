package com.mimimi.tea.yipiao.model.newsfeed;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsfeedTeaList {

    @SerializedName("featured_tea_list")
    @Expose
    private List<NewsfeedTea> featuredTeaList = null;

    public List<NewsfeedTea> getFeaturedTeaList() {
        return featuredTeaList;
    }

    public void setFeaturedTeaList(List<NewsfeedTea> featuredTeaList) {
        this.featuredTeaList = featuredTeaList;
    }
}
