package com.mimimi.tea.yipiao.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.helper.LogHelper;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.model.auth.AccessToken;
import com.mimimi.tea.yipiao.request.AuthRequest;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;

import static com.tencent.mm.opensdk.modelbase.BaseResp.ErrCode.ERR_AUTH_DENIED;
import static com.tencent.mm.opensdk.modelbase.BaseResp.ErrCode.ERR_OK;
import static com.tencent.mm.opensdk.modelbase.BaseResp.ErrCode.ERR_USER_CANCEL;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private static final String TAG = "WXEntryActivity";

    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wxentry);
        api = WXAPIFactory.createWXAPI(getApplicationContext(), Constants.APP_ID, false);
        Intent i = getIntent();
        UserHelper.addToActivitiesArray(this);
//        UserHelper.CustomProgressDialog.finishLoading();
//        UserHelper.CustomProgressDialog.startProgressDialog(this, getString(R.string.init_weixin));

        api.handleIntent(getIntent(), this);

//        LogHelper.getInstance().logEvent(TAG, "onCreate: wechat");
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
//        LogHelper.getInstance().logEvent(TAG, "onNewIntent: ");
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq baseReq) {
        LogHelper.getInstance().logEvent(TAG, "onReq: ");
    }

    @Override
    public void onResp(BaseResp baseResp) {
        LogHelper.getInstance().logEvent(TAG, "onResp: ");
        int errorCode = baseResp.errCode;

        if (baseResp instanceof SendAuth.Resp) {
            switch (errorCode) {
                case ERR_OK:
                    LogHelper.getInstance().logEvent(TAG, "onResp OK: ");
                    String code = "";

                    if (((SendAuth.Resp) baseResp).state.equals("wechat_yipiao")) {
                        code = ((SendAuth.Resp) baseResp).code;
                    }

                    new AuthRequest(this).doWeixinAuth(code, new CustomRequest.ApiResponse<AccessToken>() {
                        @Override
                        public void onSuccess(AccessToken data) {
                            LogHelper.getInstance().logEvent("checking login", "onSuccess: getWeChatAccessToken" + DateFormat.getDateTimeInstance().format(new Date()));
                            UserHelper.setUserInfo(data.getAccessToken(), "");
                            finishWithLoginResult(Constants.RESULT_LOGIN_SUCCESS);
                        }

                        @Override
                        public void onFail(String message) {
                            LogHelper.getInstance().logEvent("checking login", "failed");
                            finishWithLoginResult(Constants.RESULT_LOGIN_ERROR);
                        }
                    });
                    break;
                case ERR_AUTH_DENIED:
                    LogHelper.getInstance().logEvent(TAG, "onResp DENIED: ");
                    finishWithLoginResult(Constants.RESULT_LOGIN_ERROR);
                    break;
                case ERR_USER_CANCEL:
                    LogHelper.getInstance().logEvent(TAG, "onResp: USER CANCEL");
                    finishWithLoginResult(Constants.RESULT_LOGIN_CANCELED);
                    break;
            }
        }

        if (baseResp instanceof SendMessageToWX.Resp) {
            switch (errorCode)
            {
                case ERR_OK:
                    Toast.makeText(this, "分享成功", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(this, "分享取消", Toast.LENGTH_SHORT).show();
            }
            finish();
        }
    }

    private void finishWithLoginResult(int result) {
        //todo implement loading screen !!

//        UserHelper.CustomProgressDialog.finishLoading();
        UserHelper.clearLoginActivitiesAndHideKeyboard(this, result, Constants.LOGIN_WECHAT);
        onActivityResult(123, 13, null);
        finish();
    }
}
