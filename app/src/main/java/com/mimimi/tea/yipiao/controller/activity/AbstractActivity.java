package com.mimimi.tea.yipiao.controller.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.Services.TrackingService;
import com.mimimi.tea.yipiao.adapter.TeaDetailsRVAdapter;
import com.mimimi.tea.yipiao.controller.fragment.NoteFragment;
import com.mimimi.tea.yipiao.controller.fragment.TeaDetailsFragment;
import com.mimimi.tea.yipiao.controller.fragment.TeaSuggestFragment;
import com.mimimi.tea.yipiao.helper.LogHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.newsfeed.NewsfeedTea;
import com.mimimi.tea.yipiao.model.tea.Description;
import com.mimimi.tea.yipiao.model.tea.DescriptionItem;
import com.mimimi.tea.yipiao.model.tea.Tea;
import com.mimimi.tea.yipiao.view.TopBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

public abstract class AbstractActivity extends AppCompatActivity {

    private static final String TAG = "Base Activity";
    TrackingService trackingService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abstract);
        ViewCompat.setOnApplyWindowInsetsListener(new View(this), new OnApplyWindowInsetsListener() {
            @Override
            public WindowInsetsCompat onApplyWindowInsets(View v, WindowInsetsCompat insets) {
                return insets;
            }
        });
    // Bind to LocalService
        Intent intent = new Intent(this, TrackingService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void logTracking(int dataType, int eventType, HashMap<String, Integer> args) {
        if (mBound) {
            trackingService.addDataFootprint(dataType, eventType, args);
        } else {
            LogHelper.getInstance().logEvent(TAG, "Tracking service not initialized. Tracking not active.");
        }
    }

    /**
     * TeaDetailsFragment needs to be added to the layout for this function to work.
     *  For the fragment to function properly,
     *  findViewById(R.id.tea_details_fragment).setVisibility(View.INVISIBLE);
     *  has to be called in the onCreate of the controlling activity.
     *
     * @param tea a Tea object
     */
    public void showTeaDetails(Tea tea, boolean showFragmentBackground) {
        if (tea == null) return;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        Fragment prev = getSupportFragmentManager().findFragmentByTag("tea-details");
        if (prev != null) {
            ft.remove(prev);
        }
        TeaDetailsFragment tdf = new TeaDetailsFragment();
        tdf.setTea(tea, false, this);
        ft.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_bottom, R.anim.enter_from_bottom, R.anim.exit_to_bottom);
        ft.addToBackStack(null);
        ft.add(R.id.tea_details_fragment, tdf, "tea-details");
        ft.commit();
    }

    /**
     *
     * @param tea a Tea object
     */
    public void showNoteFragment(Tea tea) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("new-note");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack("new-note");
        String noteText = "";
        if (tea.getNote() != null) {
            noteText = tea.getNote().getText();
        }
        SupportBlurDialogFragment noteFragment = NoteFragment.newInstance(tea.getId(), tea.getName(), noteText);
        noteFragment.show(ft, "new-note");

        DialogFragment a = new DialogFragment();
    }

    /**
     *
     */
    public void showTeaSuggestFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("new-tea-suggestion");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack("new-tea-suggestion");
        SupportBlurDialogFragment teaSuggestFragment = TeaSuggestFragment.newInstance();
        teaSuggestFragment.show(ft, "new-tea-suggestion");
    }

    @Override
    public void onBackPressed() {
        /*

          Bach Button logic:
          First close share fragment if open
          then  close note fragment if open
          If neither share nor note fragment are open, then close tea details fragment
          If no fragment is open, close activity
         */
        // The list here is a list of generic fragments that close without any animations.
        // Fragments with animations have to be handled separately
        List<String> fragmentCloseOrder = Arrays.asList("new-note", "share", "tea-details");

        for (String aFragmentTag:fragmentCloseOrder) {
            getSupportFragmentManager().getFragments();
            Fragment fragmentToClose = getSupportFragmentManager().findFragmentByTag(aFragmentTag);
            if (fragmentToClose != null) {
                if (Objects.equals(aFragmentTag, "tea-details")) {
                    ((TeaDetailsFragment)fragmentToClose).setExiting();
                    super.onBackPressed();
                }
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.remove(fragmentToClose);
                ft.commit();
                //only close one fragment
                return;
            }
        }

        super.onBackPressed();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, TrackingService.class));
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            TrackingService.TrackingBinder binder = (TrackingService.TrackingBinder) service;
            trackingService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}
