package com.mimimi.tea.yipiao.request;

import android.content.Context;
import android.net.Uri;

import com.mimimi.tea.yipiao.YipiaoApplication;
import com.mimimi.tea.yipiao.model.BaseModel;
import com.mimimi.tea.yipiao.model.Note.NoteList;
import com.mimimi.tea.yipiao.model.emptyResponse.ProfileUpdate;
import com.mimimi.tea.yipiao.model.emptyResponse.TeaSuggest;
import com.mimimi.tea.yipiao.model.tea.TeaList;
import com.mimimi.tea.yipiao.model.user.BaseProfile;
import com.mimimi.tea.yipiao.model.user.UploadImage;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class ProfileRequest extends CustomRequest {

    private ProfileRequest.ApiInterface api;

    public ProfileRequest(Context context) {
        super(context);
        api = YipiaoApplication.restAdapter.create(ProfileRequest.ApiInterface.class);
    }

    private interface ApiInterface {
        @GET("/api/profile/get")
        Call<BaseModel<BaseProfile>> getBaseProfile();
        @GET("/api/profile/favourites")
        Call<BaseModel<TeaList>> getFavouriteTeas();
        @GET("/api/profile/notes")
        Call<BaseModel<NoteList>> getNotes();
        @PATCH("/api/profile/update")
        Call<BaseModel<ProfileUpdate>> update(@Body HashMap<String, Object> body);
        @POST("/api/profile/suggest-tea")
        Call<BaseModel<TeaSuggest>> suggestTea(@Body HashMap<String, Object> body);
        @Multipart
        @POST("/api/profile/update-image")
        Call<BaseModel<UploadImage>> updateImage(
                @Part MultipartBody.Part avatar
        );
    }

    public CustomRequest getBaseProfile(final ApiResponse<BaseProfile> listener) {
        completeContentCall(api.getBaseProfile(), listener);
        return this;
    }
    public CustomRequest getFavouriteTeas(final ApiResponse<TeaList> listener) {
        completeContentCall(api.getFavouriteTeas(), listener);
        return this;
    }
    public CustomRequest getNoteList(final ApiResponse<NoteList> listener) {
        completeContentCall(api.getNotes(), listener);
        return this;
    }
    public CustomRequest updateProfile(HashMap<String, Object> body, final ApiResponse<ProfileUpdate> listener) {
        completeContentCall(api.update(body), listener);
        return this;
    }
    public CustomRequest suggestTea(HashMap<String, Object> body, final ApiResponse<TeaSuggest> listener) {
        completeContentCall(api.suggestTea(body), listener);
        return this;
    }
    public CustomRequest updateImage(Uri uri, final ApiResponse<UploadImage> listener) {
        File file = new File(uri.getPath());
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/jpeg"),
                        file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
        completeContentCall(api.updateImage(body), listener);
        return this;
    }
}
