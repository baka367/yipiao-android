package com.mimimi.tea.yipiao.TeaDetailsItemDecoration;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.model.tea.Description;
import com.mimimi.tea.yipiao.model.tea.DescriptionItem;

/**
 * This decoration defines spacings for teaDetails text depending on the type and position of each block
 */
public class TeaDetailsItemDecoration extends RecyclerView.ItemDecoration {

    Context context;

    public TeaDetailsItemDecoration (Context context) {
        this.context = context;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int itemPosition = parent.getChildAdapterPosition(view);
        int viewType = parent.getAdapter().getItemViewType(itemPosition);
        boolean isLast = itemPosition == parent.getAdapter().getItemCount() - 1;

        if (itemPosition == 0 && (viewType == DescriptionItem.TYPE_IMAGE || viewType == DescriptionItem.TYPE_TEXT)) {
            outRect.top += (int) context.getResources().getDimension(R.dimen.tea_details_first_element_top_padding);
            outRect.bottom += (int) context.getResources().getDimension(R.dimen.padding10);
            return;
        }
        if (viewType == DescriptionItem.TYPE_MAIN_TITLE || viewType == DescriptionItem.TYPE_TITLE) {
            outRect.top += (int)context.getResources().getDimension(R.dimen.padding40);
            outRect.bottom += (int) context.getResources().getDimension(R.dimen.padding10);
        }
        if (viewType == DescriptionItem.TYPE_IMAGE) {
            outRect.top += (int)context.getResources().getDimension(R.dimen.padding20);
            outRect.bottom += (int) context.getResources().getDimension(R.dimen.padding20);
        }
        if (viewType == DescriptionItem.TYPE_TEXT) {
            outRect.top += (int)context.getResources().getDimension(R.dimen.padding10);
        }
        if (isLast) {
            outRect.bottom += (int)context.getResources().getDimension(R.dimen.padding40);
        }
    }
}