package com.mimimi.tea.yipiao.controller.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.adapter.ProfilePagerAdapter;
import com.mimimi.tea.yipiao.controller.activity.AbstractActivity;
import com.mimimi.tea.yipiao.controller.activity.MyProfileActivity;
import com.mimimi.tea.yipiao.helper.ImagePicker;
import com.mimimi.tea.yipiao.helper.UserHelper;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.emptyResponse.NoteWrite;
import com.mimimi.tea.yipiao.model.tea.Tea;
import com.mimimi.tea.yipiao.model.user.BaseProfile;
import com.mimimi.tea.yipiao.model.user.UploadImage;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.NoteRequest;
import com.mimimi.tea.yipiao.request.ProfileRequest;
import com.mimimi.tea.yipiao.view.TopBar;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {
    private static final String TAG = "Profile main Fragment";
    private static final int PICK_IMAGE_ID = 909;

    public ProfilePagerAdapter adapterViewPager;
    RelativeLayout profileFragment;
    TextView userNameTextView;
    CircleImageView profileImage;
    MyProfileActivity activity;
    BaseProfile profileData;
    TabLayout tabLayout;
    Uri mCropImageUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MyProfileActivity) getActivity();
        profileFragment = (RelativeLayout) inflater.inflate(R.layout.fragment_profile, container, false);;
        TopBar topBarView = new TopBar(getContext(), TopBar.LEFT_BUTTON_BACK_ACTIVITY, TopBar.RIGHT_BUTTON_SETTINGS, TopBar.MIDDLE_BUTTON_NONE);
        profileFragment.addView(topBarView);

        userNameTextView = profileFragment.findViewById(R.id.profile_user_name);
        profileImage = profileFragment.findViewById(R.id.profile_image);

        profileData = activity.getProfileData();
        createView();

        return profileFragment;
    }

    private void createView() {
        setUpUserName();
        setUpImageView();
        ViewPager vpPager = profileFragment.findViewById(R.id.profile_pager);
        adapterViewPager = new ProfilePagerAdapter(activity.getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);

        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) profileFragment.findViewById(R.id.profile_pager_header);
        tabLayout.setupWithViewPager(vpPager);

        changeTabsFont();
    }

    private void setUpUserName() {
        if (UserHelper.isGuest()) {
            userNameTextView.setBackgroundColor(activity.getResources().getColor(R.color.colorYipiaoDarkGold));
            userNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserHelper.requireLogin(getActivity(),true);
                    activity.finish();
                }
            });
        } else {
            userNameTextView.setText(UserHelper.getUsername());
            userNameTextView.setTextColor(activity.getResources().getColor(R.color.colorYipiaoGold));
        }
        userNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(UserHelper.isGuest() ? R.dimen.text_15 : R.dimen.text_17));
        userNameTextView.setTextColor(activity.getResources().getColor(UserHelper.isGuest() ? R.color.colorPureWhite : R.color.colorYipiaoGold));
        userNameTextView.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_MEDIUM));
    }

    private void setUpImageView() {
        if (UserHelper.isGuest()) {
            return;
        }
        UtilHelper.setRoundImage(getContext(), profileImage, UserHelper.getProfileImage());
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setAspectRatio(1,1)
                        .setAllowFlipping(false)
                        .setAllowRotation(false)
                        .start(activity);
            }
        });
    }

    public void updateView() {
        profileData = activity.getProfileData();
        if (profileData == null) return;
        setUpUserName();
        setUpImageView();
        adapterViewPager.setListHeaders(profileData.getFavTeaCount(), profileData.getNoteCount());
        adapterViewPager.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(activity);
            } else {
                Toast.makeText(activity, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Toast.makeText(activity, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(1,1)
                .setAllowFlipping(false)
                .setAllowRotation(false)
                .start(activity);
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof AppCompatTextView) {
                    ((TextView) tabViewChild).setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_MEDIUM));
                }
            }
        }
    }
}
