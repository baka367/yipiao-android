package com.mimimi.tea.yipiao.model.newsfeed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mimimi.tea.yipiao.model.tea.Tea;

public class NewsfeedTea {
    @SerializedName("tea")
    @Expose
    private Tea tea;
    @SerializedName("newsfeed")
    @Expose
    private NewsfeedPager newsfeed;

    public Tea getTea() {
        return tea;
    }

    public void setTea(Tea tea) {
        this.tea = tea;
    }

    public NewsfeedPager getNewsfeedPager() {
        return newsfeed;
    }

    public void setNewsfeedPager (NewsfeedPager newsfeed) {
        this.newsfeed = newsfeed;
    }

}
