package com.mimimi.tea.yipiao.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimimi.tea.yipiao.Constants;
import com.mimimi.tea.yipiao.R;
import com.mimimi.tea.yipiao.helper.UtilHelper;
import com.mimimi.tea.yipiao.model.emptyResponse.TeaSuggest;
import com.mimimi.tea.yipiao.request.CustomRequest;
import com.mimimi.tea.yipiao.request.ProfileRequest;

import java.util.HashMap;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TeaSuggestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TeaSuggestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeaSuggestFragment extends SupportBlurDialogFragment {
    private static final String TAG = "Tea suggest activity";

    private int teaId;
    private String teaName;
    private String noteText;

    Activity activity;

    private EditText teaSuggestET;

    public TeaSuggestFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NoteFragment.
     */
    public static TeaSuggestFragment newInstance() {
        TeaSuggestFragment fragment = new TeaSuggestFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        LinearLayout rootLayout = (LinearLayout) inflater.inflate(R.layout.fragment_tea_suggest, container, false);

        activity = ((Activity) getContext());

        teaSuggestET = (EditText) rootLayout.findViewById(R.id.tea_suggestion_text);
        teaSuggestET.setTypeface(UtilHelper.getFont(getContext(), Constants.FONT_PING_FANG_MEDIUM));
        teaSuggestET.setText(noteText);
        teaSuggestET.setImeOptions(EditorInfo.IME_ACTION_DONE);

        teaSuggestET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                teaSuggestET.setCursorVisible(true);
            }
        });

        TextView sendSuggestionButton = (TextView) rootLayout.findViewById(R.id.send_suggestion_button);
        sendSuggestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSuggestion();
            }
        });

        return rootLayout;
    }

    public void sendSuggestion() {
        HashMap<String, Object> teaSuggestJson = new HashMap<>();
        teaSuggestJson.put("suggestion", teaSuggestET.getText().toString());
        new ProfileRequest(getContext()).suggestTea(teaSuggestJson, new CustomRequest.ApiResponse<TeaSuggest>() {
            @Override
            public void onSuccess(TeaSuggest data) {
                if (data == null) {
                    activity.onBackPressed();
                } else {
                    //show toast that note write has failed
                    Log.e(TAG, "note write failed");
                }
            }
            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            OnFragmentInteractionListener mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onDestroyView() {
        dismiss();
        super.onDestroyView();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    protected float getDownScaleFactor() {
        return 7.0f;
    }
}
