package com.mimimi.tea.yipiao.model.newsfeed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsfeedPager {

    @SerializedName("previous_id")
    @Expose
    private Integer previousId;
    @SerializedName("next_id")
    @Expose
    private Integer nextId;
    @SerializedName("id")
    @Expose
    private Integer id;

    public Integer getPreviousId() {
        return previousId;
    }

    public void setPreviousId(Integer previousId) {
        this.previousId = previousId;
    }

    public Integer getNextId() {
        return nextId;
    }

    public void setNextId(Integer nextId) {
        this.nextId = nextId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}